/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

{

  // load the file
  TFile* f = new TFile( "gpid-1000evts.root" );

  TTree* tree = (TTree*)gDirectory->Get( "ChargedProtoTuple/protoPtuple" );

  TCut detOK = "MuonInAcc==1";

  TCut trackSel = "TrackType == 3 && TrackP > 2000 && TrackP < 100000";

  TCut realM = "fabs(MCParticleType) == 13";
  TCut fakeM = "fabs(MCParticleType) != 13";

  // new TCanvas();
  // tree->Draw( "MuonMuLL-MuonBkgLL", trackSel && detOK );

  // new TCanvas();
  // tree->Draw( "MuonMuLL", trackSel && detOK && realM );

  // new TCanvas();
  // tree->Draw( "MuonMuLL", trackSel && detOK && fakeM );

  // new TCanvas();
  // tree->Draw( "MuonBkgLL", trackSel && detOK && realM );

  // new TCanvas();
  // tree->Draw( "MuonBkgLL", trackSel && detOK && fakeM );

  // new TCanvas();
  // tree->Draw( "MuonMuLL-MuonBkgLL", trackSel && detOK && realM );

  new TCanvas();
  tree->Draw( "MuonNShared", trackSel && detOK );

  // new TCanvas();
  // tree->Draw( "CombDLLmu-CombDLLpi", trackSel && detOK && realM );

  // new TCanvas();
  // tree->Draw( "CombDLLmu-CombDLLpi", trackSel && detOK && fakeM );
}
