/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <array>
#include <map>
#include <memory>
#include <tuple>
#include <utility>
#include <vector>

#include "GlobalPID.C"

void RichKaonIDCompareFiles() {

  // const std::string dir = "/usera/jonesc/LHCb/output/U2/InclB/WithSpill";
  const std::string dir = "/usera/jonesc/LHCbCMake/Feature/Rec/output/test/RecoHits";

  // const std::string fName = "RichFuture-Feature-x86_64_v2-centos7-gcc12+detdesc-opt-Expert-ProtoTuple.root";
  const std::string fName = "RichFuture-Feature-x86_64_v3-centos7-gcc12+detdesc-opt-Expert-ProtoTuple.root";

  std::map<std::string, std::vector<std::string>> dataSets;

  auto nA = []( const std::string& label, const std::string& value ) {
    return ( value.empty() ? "" : label + "-" + value + "/" );
  };

  auto add = [&]( const auto& tag, const auto& name ) {
    auto full_name = dir + "/" + name + "/" + fName;
    boost::replace_all( full_name, "//", "/" );
    if ( boost::filesystem::exists( full_name ) ) {
      dataSets[tag].push_back( name );
    } else {
      std::cout << "WARNING: '" << full_name << "' does not exist" << std::endl;
    }
  };

  add( "Run3Tracking-3D", "RecoTracks" );
  add( "Run3Tracking-3D", "MCTracks-NoGhosts" );
  add( "Run3Tracking-3D", "MCTracks-5pcGhosts" );

  // for ( const std::string batch : {"CKRes/V2/"} ) {
  //   for ( const std::string N : {"Nominal", "Emulated-100pc", "Emulated-75pc", "Emulated-50pc", "Emulated-25pc"} ) {
  //     const auto bName = batch + N;
  //     for ( const std::string lumi : {"1.5e34", "1.2e34", "1.0e34", "3.0e33", "2.0e33"} ) {
  //       for ( const std::string innerQ : {"", "1.00"} ) {
  //         for ( const std::string outerQ : {"", "1.00"} ) {
  //           const auto d1 = nA( "lumi", lumi ) + nA( "InnerPixQ", innerQ ) + nA( "OuterPixQ", outerQ );
  //           add( bName + "/3D", bName + "/3D/" + d1 );
  //           const std::string pixWin = "1.000";
  //           for ( const std::string photWin : {"0.300", "0.150", "0.075", "0.050"} ) {
  //             const auto data = bName + "/4D/" + d1 + nA( "PixWin", pixWin ) + nA( "PhotWin", photWin );
  //             add( bName + "/4D-PhoW_" + photWin, data );
  //             add( bName + "/4D-Lumi_" + lumi, data );
  //             add( batch + "4D-Lumi_" + lumi + "-PhoW_" + photWin, data );
  //           }
  //         }
  //       }
  //     }
  //   }
  // }

  // const std::string batch       = "FromMCHits-V5";
  // const std::string innerQ      = "2.80";
  // const std::string outerQ      = "5.60";
  // const bool        addEmulated = true;
  // const auto        QS          = nA( "InnerPixQ", innerQ ) + nA( "OuterPixQ", outerQ );
  // for ( const std::string vari : {"Nominal", "MultiHits", "MultiHits-PerfectMCPos"} ) {
  //   const auto bName = batch + "-" + vari;
  //   for ( const std::string lumi : {"1.5e34", "1.2e34", "1.0e34", "3.0e33", "2.0e33"} ) {
  //     add( vari + "/3D", bName + "/3D/" + nA( "lumi", lumi ) );
  //     if ( addEmulated ) { add( vari + "/3D", "PixSize-R1R2-V1/3D/" + nA( "lumi", lumi ) + QS ); }
  //     for ( const std::string pixWin : {"1.000"} ) {
  //       for ( const std::string photWin : {"0.300", "0.150", "0.075", "0.050"} ) {
  //         auto data = bName + "/4D/" + nA( "lumi", lumi ) + nA( "PixWin", pixWin ) + nA( "PhotWin", photWin );
  //         add( vari + "/4D-PixW_" + pixWin + "-PhoW_" + photWin, data );
  //         add( vari + "/4D-PixW_" + pixWin + "-Lumi_" + lumi, data );
  //         if ( addEmulated ) {
  //           data = "PixSize-R1R2-V1/4D/" + nA( "lumi", lumi ) + QS + nA( "PixWin", pixWin ) + nA( "PhotWin", photWin
  //           ); add( vari + "/4D-PixW_" + pixWin + "-PhoW_" + photWin, data ); add( vari + "/4D-PixW_" + pixWin +
  //           "-Lumi_" + lumi, data );
  //         }
  //       }
  //     }
  //   }
  // }

  // for ( const std::string lumi : {"1.2e34", "2.0e33", "2.0e32"} ) {
  //   const std::string bName = "BkgTuneRich1";
  //   for ( const std::string w : {"0.600", "0.800", "1.000", "1.200", "1.400"} ) {
  //     dataSets["3D/lumi-" + lumi + "/" + bName].push_back( bName + "/3D/lumi-" + lumi + "/R1PixBckW-" + w );
  //     for ( const std::string pixWin : {"1.000"} ) {
  //       for ( const std::string photWin : {"0.250", "0.100", "0.050"} ) {
  //         const auto name = "4D/lumi-" + lumi + "/PixWin-" + pixWin + "/" + bName + "_PhotWin-" + photWin;
  //         dataSets[name].push_back( bName + "/4D/lumi-" + lumi + "/R1PixBckW-" + w + "/PixWin-" + pixWin +
  //         "/PhotWin-" +
  //                                   photWin );
  //       }
  //     }
  //   }
  // }

  // for ( const std::string lumi : {"1.2e34", "2.0e33", "2.0e32"} ) {
  //   const std::string bName = "BkgTuneRich2";
  //   for ( const std::string w : {"0.600", "0.800", "1.000", "1.200", "1.400"} ) {
  //     dataSets["3D/lumi-" + lumi + "/" + bName].push_back( bName + "/3D/lumi-" + lumi + "/R2PixBckW-" + w );
  //     for ( const std::string pixWin : {"1.000"} ) {
  //       for ( const std::string photWin : {"0.250", "0.100", "0.050"} ) {
  //         const auto name = "4D/lumi-" + lumi + "/PixWin-" + pixWin + "/" + bName + "_PhotWin-" + photWin;
  //         dataSets[name].push_back( bName + "/4D/lumi-" + lumi + "/R2PixBckW-" + w + "/PixWin-" + pixWin +
  //         "/PhotWin-" +
  //                                   photWin );
  //       }
  //     }
  //   }
  // }

  const Long64_t nTracks = 1e6;

  const double MeV = 1.0;
  const double GeV = 1000 * MeV;

  for ( const auto& [dname, tags] : dataSets ) {

    // make a pid object
    auto pid = std::make_unique<GlobalPID>();

    // for ( const auto tktype : {GlobalPID::Long, GlobalPID::Upstream, GlobalPID::Downstream, GlobalPID::Ttrack} ) {
    for ( const auto tktype : {GlobalPID::Long} ) {

      // Default Config Object
      GlobalPID::Configuration gConf;

      // track selection
      gConf.trackType = tktype;

      // Histo range
      if ( GlobalPID::Long == tktype ) {
        gConf.useFixedGraphRange = true;
        gConf.minGraphX          = 80;
        gConf.maxGraphX          = 100;
        gConf.minGraphY          = 0.08;
        gConf.maxGraphY          = 60;
      } else if ( GlobalPID::Upstream == tktype ) {
        gConf.useFixedGraphRange = true;
        gConf.minGraphX          = 40;
        gConf.maxGraphX          = 100;
        gConf.minGraphY          = 5;
        gConf.maxGraphY          = 50;
      } else if ( GlobalPID::Downstream == tktype ) {
        gConf.useFixedGraphRange = true;
        gConf.minGraphX          = 60;
        gConf.maxGraphX          = 100;
        gConf.minGraphY          = 1.0;
        gConf.maxGraphY          = 20;
      } else if ( GlobalPID::Ttrack == tktype ) {
        gConf.useFixedGraphRange = true;
        gConf.minGraphX          = 10;
        gConf.maxGraphX          = 100;
        gConf.minGraphY          = 1;
        gConf.maxGraphY          = 50;
      }

      // Stepping options
      gConf.minCut      = -100;
      gConf.maxCut      = 40;
      gConf.nSteps      = 100;
      gConf.minMisIDeff = gConf.minGraphY;

      // Momentum range
      if ( GlobalPID::Long == tktype ) {
        gConf.minP  = 3 * GeV;
        gConf.maxP  = 100 * GeV;
        gConf.minPt = 0.5 * GeV;
        gConf.maxPt = 100 * GeV;
      } else if ( GlobalPID::Upstream == tktype ) {
        gConf.minP  = 3 * GeV;
        gConf.maxP  = 30 * GeV;
        gConf.minPt = 0.5 * GeV;
        gConf.maxPt = 30 * GeV;
      } else if ( GlobalPID::Downstream == tktype ) {
        gConf.minP  = 3 * GeV;
        gConf.maxP  = 100 * GeV;
        gConf.minPt = 0.5 * GeV;
        gConf.maxPt = 100 * GeV;
      } else if ( GlobalPID::Ttrack == tktype ) {
        gConf.minP  = 3 * GeV;
        gConf.maxP  = 30 * GeV;
        gConf.minPt = 0.5 * GeV;
        gConf.maxPt = 30 * GeV;
      }

      // detector selection
      gConf.mustHaveAnyRICH = true;
      // Plot Type
      gConf.title     = "RICH Kaon ID";
      gConf.idType    = GlobalPID::Kaon;
      gConf.misidType = GlobalPID::Pion;
      gConf.var1      = GlobalPID::richDLLk;
      gConf.var2      = GlobalPID::richDLLpi;

      gConf.writeCutValues = false;

      using PlotData = std::vector<std::tuple<std::string, std::string, int>>;

      // colours ... https://root.cern.ch/doc/master/classTColor.html
      const auto colors =
          std::array{kRed + 1,     kBlue + 1,  kGreen + 2, kMagenta + 2, kBlack + 0, kOrange + 7, kCyan - 3,
                     kMagenta - 7, kGreen - 8, kRed - 6,   kCyan + 2,    kGray + 2,  kAzure + 8};

      auto lastColor = colors.begin();

      auto genTuple = [&]( const auto& tag ) {
        if ( colors.end() == lastColor ) { lastColor = colors.begin(); }
        auto fullname = dir + "/" + tag + "/" + fName;
        boost::replace_all( fullname, "//", "/" );
        return std::make_tuple( std::move( fullname ), tag, *( lastColor++ ) );
      };

      PlotData plotdata;
      plotdata.reserve( tags.size() );
      for ( const auto& tag : tags ) { plotdata.push_back( genTuple( tag ) ); }

      unsigned int iPlot = 0;
      for ( const auto& [fname, title, color] : plotdata ) {
        pid->loadTTree( fname );
        pid->config           = gConf;
        pid->config.imageFile = dname;
        pid->config.title += " | " + dname;
        pid->config.subtitle    = title;
        pid->config.superImpose = ( iPlot++ != 0 );
        pid->config.color       = (Color_t)color;
        // create the plot
        pid->makeCurve( nTracks );
      }

    } // track type loop

    // save the figures
    pid->saveFigures();
  }
}
