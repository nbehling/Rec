###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## @package GlobalReco
#  Base configurables for the Global ANN Charged PID
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   03/12/2010

__author__ = "Chris Jones <Christopher.Rob.Jones@cern.ch>"

from LHCbKernel.Configuration import *

# ----------------------------------------------------------------------------------


## @class ChargedProtoANNPIDConf
#  Configurable for the Global ANN Charged PID reconstruction
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   03/12/2010
class ChargedProtoANNPIDConf(LHCbConfigurableUser):

    ## Options
    __slots__ = {
        "Context": "Offline"  # The context within which to run
        ,
        "RecoSequencer": None  # The sequencer to use
        ,
        "OutputLevel": INFO  # The printout level to use
        ,
        "ProtoParticlesLocation": None,
        "NetworkVersions": {
            "Upgrade": "MCUpTuneV1",
            "DEFAULT": "MCUpTuneV1"
        },
        "DataType": ""  # Type of data, propagated from application
        ,
        "TrackTypes": ["Long", "Downstream", "Upstream"],
        "PIDTypes": ["Electron", "Muon", "Pion", "Kaon", "Proton", "Ghost"]
    }

    ## Get the ANN PID Tune for a given datatype
    def tune(self, dataType):
        nnConfigs = self.getProp("NetworkVersions")
        if dataType in nnConfigs:
            annVersion = nnConfigs[dataType]
        else:
            annVersion = nnConfigs["DEFAULT"]
            log.warning(
                "No explicit tuning for DataType='%s'. Using default '%s'" %
                (dataType, annVersion))
        return annVersion

    ## Apply the configuration to the given sequence
    def applyConf(self):

        from Configurables import ANNGlobalPID__ChargedProtoParticleAddANNPIDInfo as AddANNPidInfo
        from Configurables import ChargedProtoParticleAddInfo as AddInfoAlg

        # Check the sequencer to use is properly configured
        if not self.isPropertySet("RecoSequencer"):
            raise RuntimeError("ERROR : PROTO ANN PID Sequencer not set")
        nnpidseq = self.getProp("RecoSequencer")

        dataType = self.getProp("DataType")
        annVersion = self.tune(dataType)

        nameroot = "ANNGPID"
        if self.name() != "ChargedProtoANNPIDConf": nameroot = self.name()

        addInfoAlg = AddInfoAlg(name=nameroot, AddInfo=[])
        # Add to sequencer
        nnpidseq.Members += [addInfoAlg]

        def addANNPidInfo(TrackType, PIDType):
            t = addInfoAlg.addTool(AddANNPidInfo,
                                   nameroot + TrackType + PIDType)
            addInfoAlg.AddInfo += [t]
            # If configured, set the OutputLevel
            if self.isPropertySet("OutputLevel"):
                t.OutputLevel = self.getProp("OutputLevel")
            # Network version
            t.NetworkVersion = annVersion
            # Set configuration for this track and PID combination
            t.TrackType = TrackType
            t.PIDType = PIDType
            return t

        # Loop over track types
        for track in self.getProp("TrackTypes"):
            # Loop over PID types
            for pid in self.getProp("PIDTypes"):
                # Network tool
                addANNPidInfo(track, pid)
