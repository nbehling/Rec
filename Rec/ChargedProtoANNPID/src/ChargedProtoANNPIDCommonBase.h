/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-------------------------------------------------------------------------------
/** @file ChargedProtoANNPIDCommonBase.h
 *
 *  Declaration file for ANNGlobalPID::ChargedProtoANNPIDCommonBase
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   03/02/2011
 */
//-------------------------------------------------------------------------------
#pragma once
#include "Event/ODIN.h"
#include "Event/ProtoParticle.h"
#include "Event/RecSummary.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/FPEGuard.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/LineTypes.h"
#include "TMVA/IMethod.h"
#include "TMVA/Reader.h"
#include "TMVAIClassifierReader.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include <algorithm>
#include <cassert>
#include <map>
#include <memory>
#include <mutex>
#include <ostream>
#include <string>
#include <vector>

// This must be included after everything else because it contains a `using namespace std;`
#include "TMVAImpFactory.h"

//-----------------------------------------------------------------------------
/** @namespace ANNGlobalPID
 *
 *  General namespace for Global PID ANN software
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   2010-03-09
 */
//-----------------------------------------------------------------------------

namespace ANNGlobalPID {

  /** @class Input ChargedProtoANNPIDCommonBase.h
   *
   *  class which handles getting
   *  a particular value from a given ProtoParticle
   *
   *  @author Chris Jones
   *  @date   15/04/2013
   */

  class Input {

    struct VTable {
      float ( *value )( const void*, const LHCb::ProtoParticle* );
      void ( *move )( void*, void* ) noexcept;
      void ( *copy )( const void*, void* ) noexcept;
      void ( *dtor )( void* ) noexcept;
    };

    template <typename F>
    struct Payload {
      static constexpr float value( const void* self, const LHCb::ProtoParticle* p ) {
        return std::invoke( static_cast<const Payload*>( self )->m_f, p );
      }
      static constexpr void move( void* self, void* p ) noexcept {
        new ( p ) Payload{std::move( *static_cast<Payload*>( self ) )};
      }
      static constexpr void copy( const void* self, void* p ) noexcept {
        new ( p ) Payload{*static_cast<const Payload*>( self )};
      }
      static constexpr void dtor( void* self ) noexcept { static_cast<Payload*>( self )->~Payload(); }

      static constexpr VTable vtable{&value, &move, &copy, &dtor};
      F                       m_f;
    };

    static constexpr auto small_size = 3 * sizeof( void* );

    float ( *m_value )( const void*, const LHCb::ProtoParticle* ); // cache the hottest function pointer...
    std::aligned_storage_t<small_size> m_payload;
    std::string                        m_name;
    const VTable*                      m_vtbl;

  public:
    template <typename F, typename = std::enable_if_t<std::is_invocable_r_v<float, F, const LHCb::ProtoParticle*>>>
    Input( std::string name, F&& f ) noexcept : m_name{std::move( name )}, m_vtbl{&Payload<std::decay_t<F>>::vtable} {
      static_assert( sizeof( std::decay_t<F> ) <= sizeof( m_payload ) );
      new ( &m_payload ) Payload<std::decay_t<F>>{std::forward<F>( f )};
      m_value = m_vtbl->value;
    }

    Input( Input&& rhs ) noexcept : m_value{rhs.m_value}, m_name{std::move( rhs.m_name )}, m_vtbl{rhs.m_vtbl} {
      m_vtbl->move( &rhs.m_payload, &m_payload );
    }

    Input& operator=( Input&& rhs ) noexcept {
      if ( this == &rhs ) return *this;
      m_vtbl->dtor( &m_payload );
      m_vtbl = rhs.m_vtbl;
      m_vtbl->move( &rhs.m_payload, &m_payload );
      m_value = rhs.m_value;
      m_name  = std::move( rhs.m_name );
      return *this;
    }

    Input( Input const& rhs ) noexcept : m_value{rhs.m_value}, m_name{rhs.m_name}, m_vtbl{rhs.m_vtbl} {
      m_vtbl->copy( &rhs.m_payload, &m_payload );
    }

    Input& operator=( Input const& rhs ) noexcept {
      if ( this == &rhs ) return *this;
      m_vtbl->dtor( &m_payload );
      m_vtbl = rhs.m_vtbl;
      m_vtbl->copy( &rhs.m_payload, &m_payload );
      m_value = rhs.m_value;
      m_name  = rhs.m_name;
      return *this;
    }

    ~Input() { m_vtbl->dtor( &m_payload ); }

    /// Access the input value for a given ProtoParticle
    float operator()( const LHCb::ProtoParticle* proto ) const { return std::invoke( m_value, &m_payload, proto ); }

    /// Type for a vector of inputs
    using ConstVector = std::vector<Input>;

    /// Access the input name
    const std::string& name() const { return m_name; }
  };

  /** @class ChargedProtoANNPIDCommonBase ChargedProtoANNPIDCommonBase.h
   *
   *  Common base class
   *
   *  @author Chris Jones
   *  @date   03/02/2011
   */

  template <class PBASE>
  class ChargedProtoANNPIDCommonBase : public PBASE {

  public:
    // inherit constructors
    using PBASE::PBASE;

    /// Initialization after creation
    StatusCode initialize() override {
      return PBASE::initialize().andThen( [&] {
        // Avoid auto-loading of state provider
        m_trStateP.disable();
      } );
    }

  protected:
    /// Type for list of inputs
    using StringInputs = std::vector<std::string>;

  protected:
    /** @class Cut ChargedProtoANNPIDCommonBase.h
     *
     *  ProtoParticle selection cut
     *
     *  @author Chris Jones
     *  @date   2010-03-09
     */
    class Cut {

    public:
      /// Vector of cuts
      using ConstVector = std::vector<Cut>;

    private:
      /// Delimitor enum
      enum class Delim { UNDEFINED, GT, LT, GE, LE };

    public:
      /// Constructor
      Cut( IGeometryInfo const& geometry, std::string desc = "NOTDEFINED",
           const ChargedProtoANNPIDCommonBase<PBASE>* parent = nullptr );
      /// No Copy Constructor
      Cut( const Cut& ) = delete;
      Cut& operator=( const Cut& ) = delete;
      Cut( Cut&& )                 = default;
      Cut& operator=( Cut&& ) = default;

    public:
      /// Is this object well defined
      bool isOK() const noexcept { return m_OK; }
      /// Does the ProtoParticle pass the cut
      bool isSatisfied( const LHCb::ProtoParticle* proto ) const {
        const auto var = m_variable( proto );
        switch ( m_delim ) {
        case Delim::GT:
          return var > m_cut;
        case Delim::LT:
          return var < m_cut;
        case Delim::GE:
          return var >= m_cut;
        case Delim::LE:
          return var <= m_cut;
        default:
          return false;
        }
      }
      /// Cut description
      const std::string description() const noexcept { return m_desc; }

    public:
      /// Overload output to ostream
      friend std::ostream& operator<<( std::ostream& s, const Cut& cut ) {
        return s << "'" << cut.description() << "'";
      }

    private:
      /// Set the delimitor enum from a string
      bool setDelim( const std::string& delim ) noexcept {
        // clang-format off
        m_delim = ( delim == ">"  ? Delim::GT
                  : delim == "<"  ? Delim::LT
                  : delim == ">=" ? Delim::GE
                  : delim == "<=" ? Delim::LE
                  : Delim::UNDEFINED );
        // clang-format on
        return m_delim != Delim::UNDEFINED;
      }

    private:
      std::string m_desc;                    ///< The cut description
      Input       m_variable;                ///< The variable to cut on
      float       m_cut{0};                  ///< The cut value
      Delim       m_delim{Delim::UNDEFINED}; ///< The delimitor
      bool        m_OK{false};               ///< Is this cut well defined
    };

  protected:
    /** @class ANNHelper ChargedProtoANNPIDCommonBase.h
     *
     *  Base class for ANN helpers
     *
     *  @author Chris Jones
     *  @date   2010-03-09
     */
    class ANNHelper {

    protected:
      /// Type for list of inputs
      using Inputs = typename Input::ConstVector;

    public:
      /** Constructor from information
       *  @param inputs The list of inputs needed for this network
       */
      ANNHelper( Inputs inputs ) : m_inputs{std::move( inputs )} {}

      /// Destructor
      virtual ~ANNHelper() = default;

      /// Are we configured properly
      bool isOK() const noexcept { return m_mva && m_mva->IsStatusClean(); }

      /// Type for input values
      using InVars = std::vector<float>;

      /// Access the inputs
      const Inputs& inputs() const noexcept { return m_inputs; }

      /// Generate input storage vector
      decltype( auto ) inputStorage() const { return InVars( inputs().size(), 0 ); }

      /// Fill the input variables for given ProtoParticle
      void fillInputVars( const LHCb::ProtoParticle* proto, InVars& vars ) const {
        const auto& ins = this->inputs();
        assert( vars.size() == ins.size() );
        std::transform( ins.begin(), ins.end(), vars.begin(),
                        [&proto]( const auto& input ) { return input( proto ); } );
      }

    private:
      /// Compute the ANN output for the given variables
      decltype( auto ) getOutput( const InVars& vars ) const {
        // FPE Guard for MVA call
        FPE::Guard guard( true );
        // call the specific IClassifierReader instance to use
        return m_mva->GetMvaValue( vars );
      }

    public:
      /// Compute the ANN output for the given ProtoParticle using the given input storage
      decltype( auto ) getOutput( const LHCb::ProtoParticle* proto, InVars& vars ) const {
        // fill inputs
        fillInputVars( proto, vars );
        // compute MVA and return
        return getOutput( vars );
      }

      /// Compute the ANN output for the given ProtoParticle
      decltype( auto ) getOutput( const LHCb::ProtoParticle* proto ) const {
        // input vars storage
        auto vars = inputStorage();
        // compute MVA and return
        return getOutput( proto, vars );
      }

    private:
      /// The list of inputs for this network
      Inputs m_inputs;

    protected:
      /// Pointer to the underlying MVA implementation. Set by specific implementation.
      IClassifierReader* m_mva{nullptr};
    };

    /** @class TMVAReaderANN ChargedProtoANNPIDCommonBase.h
     *
     *  Helper class for TMVA ANN networks via the Reader
     *
     *  @author Chris Jones
     *  @date   2013-03-09
     */
    class TMVAReaderANN final : public ANNHelper, public IClassifierReader {

    public:
      /// No default constructor
      TMVAReaderANN() = delete;

    public:
      /** Constructor from information
       *  @param paramFileName Network tuning parameter file
       *  @param inputs        The list of inputs needed for this network
       *  @param parent        Pointer to parent algorithm
       */
      TMVAReaderANN( std::string const& paramFileName, StringInputs const& inputs,
                     ChargedProtoANNPIDCommonBase<PBASE> const* parent, IGeometryInfo const& geometry )
          : ANNHelper{parent->getInputs( inputs, geometry )}
          , m_reader(
                std::make_unique<TMVA::Reader>( parent->msgLevel( MSG::DEBUG ) ? "!Color:!Silent" : "!Color:Silent" ) )
          , m_vars( inputs.size(), 0 ) {
        // init the inputs
        unsigned int i = 0;
        for ( const auto& input : inputs ) { m_reader->AddVariable( input.c_str(), &( m_vars[i++] ) ); }
        m_reader->BookMVA( "PID", paramFileName.c_str() );
        // check and set status
        this->fStatusIsClean = m_reader && m_reader->FindMVA( "PID" );
        // set the pointer to the IClassifierReader instance used by the public API
        this->m_mva = this;
      }

    private:
      /// Compute the ANN output for the given vector of inputs
      float GetMvaValue( const typename ANNHelper::InVars& vars ) const override;

    private:
      std::unique_ptr<TMVA::Reader> m_reader;   ///< The TMVA reader
      mutable std::vector<float>    m_vars;     ///< Working array for network inputs
      mutable std::mutex            m_varsLock; ///< mutex lock
    };

    /** @class TMVAImpANN ChargedProtoANNPIDCommonBase.h
     *
     *  Helper class for TMVA ANN networks via the C++ implementation
     *
     *  @author Chris Jones
     *  @date   2013-03-09
     */
    class TMVAImpANN final : public ANNHelper {

    public:
      /** Constructor from information
       *  @param config   The tune name
       *  @param particle The particle type
       *  @param track    The track type
       *  @param inputs   The list of inputs needed for this network
       *  @param parent   Pointer to parent algorithm
       */
      TMVAImpANN( std::string const& config, std::string const& particle, std::string const& track,
                  StringInputs const& inputs, ChargedProtoANNPIDCommonBase<PBASE> const* parent,
                  IGeometryInfo const& geometry )
          : ANNHelper{parent->getInputs( inputs, geometry )} //
          , m_reader( tmvaFactory().create( config, particle, track, inputs ) ) {
        // set the pointer to the IClassifierReader instance used by the public API
        this->m_mva = m_reader.get();
      }

    private:
      std::unique_ptr<IClassifierReader> m_reader; ///< The owned compiled TMVA instance
    };

  protected:
    /** @class NetConfig ChargedProtoANNPIDCommonBase.h
     *
     *  Helper class that encapsulates the configration of an ANN nework
     *
     *  @author Chris Jones
     *  @date   2014-06-27
     */
    class NetConfig final {

    public:
      /// Constructor
      NetConfig( std::string const& trackType, std::string const& pidType, std::string const& netVersion,
                 ChargedProtoANNPIDCommonBase<PBASE> const* parent, IGeometryInfo const& geometry );

      /// Access the Network object
      decltype( auto ) netHelper() const noexcept { return m_netHelper.get(); }

      /// Status
      bool isOK() const noexcept { return netHelper() && netHelper()->isOK(); }

      /// Access the track type
      const std::string& trackType() const noexcept { return m_trackType; }

      /// Access the particle type
      const std::string& particleType() const noexcept { return m_particleType; }

      /// Check a ProtoParticle against the configured cuts
      bool passCuts( const LHCb::ProtoParticle* proto ) const noexcept {
        return std::all_of( m_cuts.begin(), m_cuts.end(), //
                            [&proto]( const auto& cut ) { return cut.isSatisfied( proto ); } );
      }

    private:
      /// Clean up
      void cleanUp() {
        m_netHelper.reset();
        m_cuts.clear();
        m_particleType = "";
        m_trackType    = "";
      }

    private:
      /// Network Helper
      std::unique_ptr<const ANNHelper> m_netHelper;

      /// Vector of cuts to apply
      typename Cut::ConstVector m_cuts;

      /// The particle type
      std::string m_particleType;

      /// The track type
      std::string m_trackType;
    };

  public:
    /** Get the Input object for a given input name
     */
    virtual Input getInput( std::string const& name, IGeometryInfo const& geometry ) const;

    /** Get a vector of input objects for a given set of names
     */
    typename Input::ConstVector getInputs( StringInputs const& names, IGeometryInfo const& geometry ) const;

  protected:
    /// Track state provider
    ToolHandle<ITrackStateProvider> m_trStateP{this, "TrackStateProvider", "TrackStateProvider/StateProvider"};
  };

} // namespace ANNGlobalPID
