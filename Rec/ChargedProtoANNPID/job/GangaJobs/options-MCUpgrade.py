###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import (DaVinci, GaudiSequencer, LHCbApp, CondDB)
from Configurables import (DecayTreeTuple, LoKi__Hybrid__TupleTool,
                           TupleToolANNPID)

#from Configurables import ( ANNGlobalPID__ChargedProtoANNPIDTrainingTuple )
#pidtuple = ANNGlobalPID__ChargedProtoANNPIDTrainingTuple("ANNPID")
#pidtuple.NTupleLUN = "ANNPIDTUPLE"
#DaVinci().UserAlgorithms += [ pidtuple ]

tuple = DecayTreeTuple("ANNPID")
tuple.Decay = "[pi+]cc"
tuple.NTupleLUN = "ANNPIDTUPLE"
tuple.Inputs = [
    'Phys/StdAllNoPIDsPions/Particles', 'Phys/StdNoPIDsUpPions/Particles',
    'Phys/StdNoPIDsDownPions/Particles'
]
tuple.ToolList = ["TupleToolANNPIDTraining", "TupleToolGeometry"]

annTool = tuple.addTupleTool(TupleToolANNPID)
annTool.ANNPIDTunes = ["MC15TuneV1"]

#lokiT = tuple.addTupleTool( LoKi__Hybrid__TupleTool, name = "LokiTool" )
#lokiT.Variables = { "MIPCHI2_PRIMARY" : "MIPCHI2DV(PRIMARY)" }

DaVinci().UserAlgorithms += [tuple]

DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000

#DaVinci().EvtMax     = 1000
#DaVinci().PrintFreq  = 100

#DaVinci().SkipEvents = 100000

DaVinci().InputType = 'DST'
DaVinci().Simulation = True

DaVinci().DataType = "Upgrade"
LHCbApp().DataType = "Upgrade"
from Configurables import CondDB
CondDB().setProp("Upgrade", True)

NTupleSvc().Output += [
    "ANNPIDTUPLE DATAFILE='ProtoPIDANN.MC.tuples.root' TYP='ROOT' OPT='NEW'"
]

from Configurables import LHCbApp
LHCbApp().TimeStamp = True

# Set the compression level for the ROOT tuple file
from GaudiKernel.Configurable import ConfigurableGeneric as RFileCnv
RFileCnv('RFileCnv').GlobalCompression = "LZMA:6"

# tags
LHCbApp().DDDBtag = "dddb-20171126"
LHCbApp().CondDBtag = "sim-20171127-vc-md100"

#EventSelector().Input = ["DATAFILE='PFN:/usera/jonesc/NFS/data/MC/Upgrade/Sim09c-Up02/Reco-Up01/LDST/00071353_00000673_2.ldst' TYP='POOL_ROOTTREE' OPT='READ'"]
