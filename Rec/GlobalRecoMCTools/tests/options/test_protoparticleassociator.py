###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the charged and neutral variants of the ProtoParticleAssociator algorithm.

For charged protos, this looks like:

1. Process input data that contains Track -> MCParticle LinksByKey objects.
2. Run the ChargedProtoParticleAssociator algorithm which takes as input:
  - The tracks used to used the LinksByKey object.
  - The MC particles used to used the LinksByKey object.
  - The LinksByKey objects.
3. Assert that the LinksByKey and Relation1DWeighted objects produced by
ChargedProtoParticleAssociator contain the correct ProtoParticle ->
MCParticle links.

There's similar procedure for neutral protos, using CaloHypo -> MCParticle
link objects and the NeutralProtoParticleAssociator algorithm instead.
"""
from __future__ import print_function

from Configurables import (
    ApplicationMgr,
    ChargedProtoParticleAssociator,
    LHCbApp,
    MergeLinksByKeysToVector,
    NeutralProtoParticleAssociator,
    UnpackCaloHypo,
    UnpackMCParticle,
    UnpackProtoParticle,
    UnpackTrack,
)
import GaudiPython as GP
from PRConfig.TestFileDB import test_file_db
from DDDB.CheckDD4Hep import UseDD4Hep


def compare(protos, pp2mcp, link_paths, charged):
    nmatches = 0
    ref = GP.gbl.LHCb.LinkReference()
    for proto in protos.containedObjects():
        rels = pp2mcp.relations(proto)
        # It would be nicer to use sets here, but the objects we retrieve from
        # the tables are pointers, so their hashes may be different and set
        # membership checks would fail. Instead, we use a list and fall back to
        # O(n) lookup.
        pairs = []
        for idx in range(len(rels)):
            rel = rels.at(idx)
            pairs.append((getattr(rel, "from")(), rel.to()))

        basics = proto.calo() if not charged else [proto.track()]
        for jdx in range(len(basics)):
            basic = basics[jdx]
            for link_path in link_paths:
                links = TES[str(link_path)]
                ok = links.firstReference(basic.index(), basic.parent(), ref)
                while ok:
                    to_path = links.linkMgr().link(ref.linkID()).path()
                    to_index = ref.objectKey()
                    mcp = TES[to_path].object(to_index)
                    assert (
                        proto, mcp
                    ) in pairs, "Basic -> MCP link not found in ProtoParticle -> MCP table"
                    nmatches += 1
                    ok = links.nextReference(ref)

    return nmatches


app = LHCbApp(EvtMax=100)
test_file_db["upgrade_minbias_hlt1_filtered"].run(configurable=app)
if UseDD4Hep:
    from Configurables import (
        LHCb__Det__LbDD4hep__IOVProducer as IOVProducer,
        LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc,
        LHCb__Tests__FakeRunNumberProducer as FET,
    )
    DD4hepSvc(DetectorList=[])
    cond_algs = [
        FET('FakeRunNumber', ODIN='DummyODIN', Start=0, Step=0),
        IOVProducer("ReserveIOVDD4hep", ODIN='DummyODIN'),
    ]
else:
    from Configurables import CondDB
    CondDB(Upgrade=True)
    cond_algs = []

unpack_mcp = UnpackMCParticle()
unpack_track = UnpackTrack()
unpack_charged_pp = UnpackProtoParticle()
hypo_locs = [
    "Rec/Calo/Photons", "Rec/Calo/SplitPhotons", "Rec/Calo/MergedPi0s"
]
unpack_calohypos = [
    UnpackCaloHypo(
        "Unpack{}".format(loc.replace("/", "")),
        InputName="p" + loc,
        OutputName=loc) for loc in hypo_locs
]
unpack_neutral_pp = UnpackProtoParticle(
    name="UnpackProtoParticleNeutrals",
    InputName="pRec/ProtoP/Neutrals",
    OutputName="Rec/ProtoP/Neutrals")

charged_merger = MergeLinksByKeysToVector(
    "ChargedMergeLinksByKeysToVector",
    InputLinksByKeys=["Link/" + str(unpack_track.getProp("OutputName"))],
    Output="Link/MergedTrack2MCP",
)
charged_assoc = ChargedProtoParticleAssociator(
    InputProtoParticles=unpack_charged_pp.getProp("OutputName"),
    InputMCParticles=unpack_mcp.getProp("OutputName"),
    InputAssociations=charged_merger.Output,
    OutputLinks="Link/ChargedPP2MCP",
    OutputTable="Relations/ChargedPP2MCP",
)

neutral_merger = MergeLinksByKeysToVector(
    "NeutralMergeLinksByKeysToVector",
    InputLinksByKeys=[
        "Link/" + str(alg.getProp("OutputName")) for alg in unpack_calohypos
    ],
    Output="Link/MergedCaloHypo2MCP",
)
neutral_assoc = NeutralProtoParticleAssociator(
    InputProtoParticles=unpack_neutral_pp.getProp("OutputName"),
    InputMCParticles=unpack_mcp.getProp("OutputName"),
    InputAssociations=neutral_merger.Output,
    OutputLinks="Link/NeutralPP2MCP",
    OutputTable="Relations/NeutralPP2MCP",
)

ApplicationMgr(TopAlg=cond_algs + unpack_calohypos + [
    unpack_neutral_pp,
    unpack_track,
    unpack_charged_pp,
    unpack_mcp,
    charged_merger,
    charged_assoc,
    neutral_merger,
    neutral_assoc,
])

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

i = 0
nmatches_charged = 0
nmatches_neutral = 0
while TES["/Event"] and i < app.EvtMax:
    charged_protos = TES[str(unpack_charged_pp.getProp("OutputName"))]
    charged_pp2mcp = TES[str(charged_assoc.getProp("OutputTable"))]
    charged_link_paths = charged_merger.getProp("InputLinksByKeys")
    nmatches_charged += compare(
        charged_protos, charged_pp2mcp, charged_link_paths, charged=True)

    neutral_protos = TES[str(unpack_neutral_pp.getProp("OutputName"))]
    neutral_pp2mcp = TES[str(neutral_assoc.getProp("OutputTable"))]
    neutral_link_paths = neutral_merger.getProp("InputLinksByKeys")
    nmatches_neutral += compare(
        neutral_protos, neutral_pp2mcp, neutral_link_paths, charged=False)

    appMgr.run(1)
    i += 1

assert nmatches_charged > 0, "ERROR: Found zero charged matches"
assert nmatches_neutral > 0, "ERROR: Found zero neutral matches"
print("SUCCESS: Found {}/{} charged/neutral matches".format(
    nmatches_charged, nmatches_neutral))
