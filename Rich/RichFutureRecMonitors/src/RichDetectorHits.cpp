/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/RichDAQDefinitions.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// AIDA
#include "AIDA/IAxis.h"

// boost
#include "boost/container/small_vector.hpp"

// STD
#include <algorithm>
#include <cstdint>
#include <mutex>
#include <vector>

namespace Rich::Future::Rec::Moni {

  /** @class DetectorHits RichDetectorHits.h
   *
   *  Monitors the data sizes in the RICH detectors.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class DetectorHits final : public LHCb::Algorithm::Consumer<void( const SIMDPixelSummaries& ), //
                                                              Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    DetectorHits( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins2DHistos", 50 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const SIMDPixelSummaries& pixels ) const override {

      // the lock
      std::lock_guard lock( m_updateLock );

      // Loop over RICHes
      for ( const auto rich : activeDetectors() ) {

        // ID for last seen PD
        LHCb::RichSmartID lastPD;

        // count hits in current PD
        std::size_t curPDhits{0};

        // cache iX and iY indices for hits in current PD
        boost::container::small_vector<std::pair<int, int>, LHCb::RichSmartID::MaPMT::TotalPixels> hitiXiY;

        // count for occupancy in each PD
        using VD = std::vector<double>;
        std::vector<VD> pdOccXY{nBins2D(), VD( nBins2D(), 0.0 )};

        // Count active PDs in each RICH
        std::uint32_t activePDs = 0;

        // functor to fill data for a given PD
        auto newPD = [&]() {
          // If last PD ID is valid fill plots etc. for the last PD
          if ( lastPD.isValid() ) {
            // Fill average HPD occ plot
            fillHisto( h_nTotalPixsPerPD[rich], curPDhits );
            if ( Rich::Rich2 == rich ) {
              fillHisto( ( lastPD.isHTypePMT() ? h_nTotalPixsPerPDR2H : h_nTotalPixsPerPDR2R ), curPDhits );
            }
            // PD occ
            for ( const auto& [iX, iY] : hitiXiY ) {
              pdOccXY[iX][iY] = ( 100.0 * curPDhits ) / (double)LHCb::RichSmartID::MaPMT::TotalPixels;
            }
          }
          // reset caches
          hitiXiY.clear();
          curPDhits = 0;
        };

        // Fill RICH hits plots
        const auto nHits = pixels.nHitsScalar( rich );
        if ( nHits > 0 ) { fillHisto( h_nTotalPixs[rich], nHits ); }

        // Loop over pixels for this RICH
        for ( const auto& pix : pixels.range( rich ) ) {
          // scalar loop
          for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i ) {
            if ( !pix.validMask()[i] ) { continue; }

            // Hit data
            const auto id   = pix.smartID()[i];
            const auto pdID = id.pdID();
            const auto side = pdID.panel();

            // If new PD fill plots and caches
            if ( lastPD != pdID ) {
              newPD();
              lastPD = pdID;
              // count active PDs
              ++activePDs;
            }

            // Count hits in current PD
            ++curPDhits;

            // get global/local coords
            const auto gPos = pix.gloPos( i );
            const auto lPos = pix.locPos( i );

            // fill plots
            fillHisto( h_pixXYGlo[rich][side], gPos.X(), gPos.Y() );
            fillHisto( h_pixXGlo[rich][side], gPos.X() );
            fillHisto( h_pixYGlo[rich][side], gPos.Y() );
            fillHisto( h_pixXYLoc[rich], lPos.X(), lPos.Y() );
            fillHisto( h_pixXLoc[rich], lPos.X() );
            fillHisto( h_pixYLoc[rich], lPos.Y() );

            // cache occupancy data
            const auto iX = std::clamp( h_pdXYOcc[rich]->xAxis().coordToIndex( lPos.X() ), 0, (int)nBins2D() - 1 );
            const auto iY = std::clamp( h_pdXYOcc[rich]->yAxis().coordToIndex( lPos.Y() ), 0, (int)nBins2D() - 1 );
            hitiXiY.emplace_back( iX, iY );

          } // scalar loop
        }   // SIMD pixel loop

        // call once for last hit
        newPD();

        // Fill occupancies
        for ( auto iX = 0u; iX < nBins2D(); ++iX ) {
          for ( auto iY = 0u; iY < nBins2D(); ++iY ) {
            const auto X   = 0.5 * ( h_pdXYOcc[rich]->xAxis().binLowerEdge( iX ) + //
                                   h_pdXYOcc[rich]->xAxis().binUpperEdge( iX ) );
            const auto Y   = 0.5 * ( h_pdXYOcc[rich]->yAxis().binLowerEdge( iY ) + //
                                   h_pdXYOcc[rich]->yAxis().binUpperEdge( iY ) );
            const auto occ = pdOccXY[iX][iY];
            fillHisto( h_pdXYOcc[rich], X, Y, occ );
            fillHisto( h_pdXOcc[rich], X, occ );
            fillHisto( h_pdYOcc[rich], Y, occ );
          }
        }

        // Fill active PD plots
        fillHisto( h_nActivePDs[rich], activePDs );

      } // RICHes
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      const DetectorArray<double> panelXsizes{{650, 800}};
      const DetectorArray<double> panelYsizes{{680, 750}};
      const DetectorArray<double> gloXsizes{{750, 4100}};
      const DetectorArray<double> gloYsizes{{1600, 800}};
      const DetectorArray<double> pSF{{0.6, 0.5}}; // reduced (x,y) size in global projection

      for ( const auto rich : activeDetectors() ) {
        ok &= saveAndCheck( h_nTotalPixsPerPD[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nTotalPixsPerPD", rich ), //
                                         "Average overall PD occupancy (nHits>0)",     //
                                         0.5, 64.5, 64 ) );
        if ( Rich::Rich2 == rich ) {
          ok &= saveAndCheck( h_nTotalPixsPerPDR2H,                                           //
                              richHisto1D( Rich::HistogramID( "nTotalPixsPerHTypePD", rich ), //
                                           "Average overall H-Type PD occupancy (nHits>0)",   //
                                           0.5, 64.5, 64 ) );
          ok &= saveAndCheck( h_nTotalPixsPerPDR2R,                                           //
                              richHisto1D( Rich::HistogramID( "nTotalPixsPerRTypePD", rich ), //
                                           "Average overall R-Type PD occupancy (nHits>0)",   //
                                           0.5, 64.5, 64 ) );
        }
        ok &= saveAndCheck( h_nTotalPixs[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nTotalPixs", rich ), //
                                         "Overall occupancy (nHits>0)",           //
                                         0.5, m_maxPixels[rich] + 0.5, m_maxPixels[rich] / 100 ) );
        ok &= saveAndCheck( h_nActivePDs[rich],                                   //
                            richHisto1D( Rich::HistogramID( "nActivePDs", rich ), //
                                         "# Active PDs (nHits>0)", -0.5, 2000.5, 2001 ) );
        // hit points (global)
        for ( const auto side : Rich::sides() ) {
          const auto minX =
              ( rich == Rich::Rich1 ? -gloXsizes[rich]
                                    : side == Rich::right ? -gloXsizes[rich] //
                                                          : gloXsizes[rich] - ( pSF[rich] * panelXsizes[rich] ) );
          const auto maxX =
              ( rich == Rich::Rich1 ? gloXsizes[rich]
                                    : side == Rich::right ? ( pSF[rich] * panelXsizes[rich] ) - gloXsizes[rich] //
                                                          : gloXsizes[rich] );
          const auto minY =
              ( rich == Rich::Rich2 ? -gloYsizes[rich]
                                    : side == Rich::top ? gloYsizes[rich] - ( pSF[rich] * panelYsizes[rich] ) //
                                                        : -gloYsizes[rich] );
          const auto maxY =
              ( rich == Rich::Rich2 ? gloYsizes[rich]
                                    : side == Rich::top ? gloYsizes[rich] //
                                                        : -gloYsizes[rich] + ( pSF[rich] * panelYsizes[rich] ) );
          ok &= saveAndCheck( h_pixXYGlo[rich][side],                     //
                              richHisto2D( HID( "pixXYGlo", rich, side ), //
                                           "Global (X,Y) hits",           //
                                           minX, maxX, nBins2D(),         //
                                           minY, maxY, nBins2D(),         //
                                           "Global X / mm", "Global Y / mm" ) );
          ok &= saveAndCheck( h_pixXGlo[rich][side],                     //
                              richHisto1D( HID( "pixXGlo", rich, side ), //
                                           "Global X hits",              //
                                           minX, maxX, nBins2D(),        //
                                           "Global X / mm" ) );
          ok &= saveAndCheck( h_pixYGlo[rich][side],                     //
                              richHisto1D( HID( "pixYGlo", rich, side ), //
                                           "Global Y hits",              //
                                           minY, maxY, nBins2D(),        //
                                           "Global Y / mm" ) );
        }
        // hit points (local)
        ok &= saveAndCheck( h_pixXYLoc[rich],                                              //
                            richHisto2D( HID( "pixXYLoc", rich ),                          //
                                         "Local (X,Y) hits",                               //
                                         -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                                         -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                                         "Local X / mm", "Local Y / mm" ) );
        ok &= saveAndCheck( h_pixXLoc[rich],                                               //
                            richHisto1D( HID( "pixXLoc", rich ),                           //
                                         "Local X hits",                                   //
                                         -panelXsizes[rich], panelXsizes[rich], nBins1D(), //
                                         "Local X / mm" ) );
        ok &= saveAndCheck( h_pixYLoc[rich],                                               //
                            richHisto1D( HID( "pixYLoc", rich ),                           //
                                         "Local Y hits",                                   //
                                         -panelYsizes[rich], panelYsizes[rich], nBins1D(), //
                                         "Local Y / mm" ) );
        ok &= saveAndCheck( h_pdXYOcc[rich],                                                 //
                            richProfile2D( HID( "pdOccXYLoc", rich ),                        //
                                           "PD Average Occupancy (X,Y)",                     //
                                           -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                                           -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                                           "Local X / mm", "Local Y / mm", "Occupancy / %" ) );
        ok &= saveAndCheck( h_pdXOcc[rich],                                                  //
                            richProfile1D( HID( "pdOccXLoc", rich ),                         //
                                           "PD Average Occupancy (X)",                       //
                                           -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                                           "Local X / mm", "Occupancy / %" ) );
        ok &= saveAndCheck( h_pdYOcc[rich],                                                  //
                            richProfile1D( HID( "pdOccYLoc", rich ),                         //
                                           "PD Average Occupancy (Y)",                       //
                                           -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                                           "Local Y / mm", "Occupancy / %" ) );
      }

      return StatusCode{ok};
    }

  private:
    /// Maximum pixels
    Gaudi::Property<DetectorArray<unsigned int>> m_maxPixels{this, "MaxPixels", {30000u, 20000u}};

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;

  private:
    // cached histograms

    /// Pixels per PD histograms
    DetectorArray<AIDA::IHistogram1D*> h_nTotalPixsPerPD    = {{}};
    AIDA::IHistogram1D*                h_nTotalPixsPerPDR2H = nullptr;
    AIDA::IHistogram1D*                h_nTotalPixsPerPDR2R = nullptr;
    /// Pixels per detector histograms
    DetectorArray<AIDA::IHistogram1D*> h_nTotalPixs = {{}};
    /// Number active PDs histograms
    DetectorArray<AIDA::IHistogram1D*> h_nActivePDs = {{}};

    DetectorArray<PanelArray<AIDA::IHistogram2D*>> h_pixXYGlo = {{}};
    DetectorArray<PanelArray<AIDA::IHistogram1D*>> h_pixXGlo  = {{}};
    DetectorArray<PanelArray<AIDA::IHistogram1D*>> h_pixYGlo  = {{}};

    DetectorArray<AIDA::IHistogram2D*> h_pixXYLoc = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_pixXLoc  = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_pixYLoc  = {{}};

    DetectorArray<AIDA::IProfile2D*> h_pdXYOcc = {{}};
    DetectorArray<AIDA::IProfile1D*> h_pdXOcc  = {{}};
    DetectorArray<AIDA::IProfile1D*> h_pdYOcc  = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DetectorHits )

} // namespace Rich::Future::Rec::Moni
