###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureRecTools
-----------------------
#]=======================================================================]

gaudi_add_module(RichFutureRecTools
    SOURCES
        src/RichPIDPlots.cpp
    LINK
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::RichFutureRecBase
        Rec::RichFutureRecInterfacesLib
)

# Fixes for GCC7.
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "7.0")
    target_compile_options(RichFutureRecTools PRIVATE -faligned-new)
endif()
