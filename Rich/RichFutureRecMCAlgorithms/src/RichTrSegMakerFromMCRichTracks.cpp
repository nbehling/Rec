/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/LinksByKey.h"
#include "Event/MCRichSegment.h"
#include "Event/MCRichTrack.h"
#include "Event/MCTrackInfo.h"
#include "Event/State.h"
#include "Event/StateVector.h"
#include "Event/Track.h"

// Rich Utils
#include "RichUtils/BoostArray.h"
#include "RichUtils/RichRayTracingUtils.h"
#include "RichUtils/RichTrackSegment.h"

// Rich Detector
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"

// LHCbKernel
#include "Kernel/RichSmartID.h"

// Rec event model
#include "RichFutureRecEvent/RichRecRelations.h"

// Detector elements
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IGeometryInfo.h"

// Track Tools
#include "MCInterfaces/IIdealStateCreator.h"
#include "PrKernel/PrChecker.h"

// STL
#include <array>
#include <memory>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

// pull in methods from Rich::RayTracingUtils
using namespace Rich::RayTracingUtils;

namespace Rich::Future::Rec::MC {

  namespace {

    /// The output data
    using OutData = std::tuple<LHCb::RichTrackSegment::Vector,     //
                               Relations::TrackToSegments::Vector, //
                               Relations::SegmentToTrackVector,    //
                               LHCb::Tracks,                       //
                               LHCb::LinksByKey>;

    /// cached Detector information
    class SegMakerDetInfo {
    public:
      /// Type for pointers to RICH radiator detector elements
      using Radiators = std::vector<const Rich::Detector::Radiator*>;

    public:
      /// Rich1 and Rich2 detector elements
      DetectorArray<const Rich::Detector::RichBase*> riches = {{}};
      /// Pointers to RICH radiator detector elements
      Radiators radiators;

    public:
      /// Construct from detector elements
      SegMakerDetInfo( const Rich::Detector::Rich1&     rich1, //
                       const Rich::Detector::Rich2&     rich2, //
                       const Rich::RadiatorArray<bool>& usedRads ) {
        riches = {&rich1, &rich2};
        radiators.reserve( Rich::NRiches );
        if ( usedRads[Rich::Rich1Gas] ) { radiators.push_back( &rich1.radiator() ); }
        if ( usedRads[Rich::Rich2Gas] ) { radiators.push_back( &rich2.radiator() ); }
      }
    };

  } // namespace

  /** @class TrSegMakerFromMCRichTracks
   *
   *  Builds RichTrackSegments from MCRichTracks
   *
   *  @author Chris Jones
   *  @date   2023-06-29
   */
  class TrSegMakerFromMCRichTracks final : public LHCb::Algorithm::MultiTransformer<
                                               OutData( LHCb::MCParticles const&,  //
                                                        LHCb::MCRichTracks const&, //
                                                        LHCb::MCProperty const&,   //
                                                        SegMakerDetInfo const&,    //
                                                        DetectorElement const& ),
                                               LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, //
                                                                                    SegMakerDetInfo, DetectorElement>> {

  public:
    /// Standard constructor
    TrSegMakerFromMCRichTracks( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // data inputs
                            {KeyValue{"MCParticleLocation", LHCb::MCParticleLocation::Default},
                             KeyValue{"MCRichTracksLocation", LHCb::MCRichTrackLocation::Default},
                             KeyValue{"MCPropertyLocation", LHCb::MCPropertyLocation::TrackInfo},
                             // conditions input
                             KeyValue{"DetectorCache", DeRichLocations::derivedCondition( name + "-DetectorCache" )},
                             KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}},
                            // data outputs
                            {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                             KeyValue{"TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Initial},
                             KeyValue{"SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default},
                             KeyValue{"OutputTracksLocation", LHCb::TrackLocation::Default + "RICH"},
                             KeyValue{"MCParticlesLinkLocation", "Link/" + LHCb::TrackLocation::Default + "RICH"}} ) {}

    /// Initialization after creation
    StatusCode initialize() override {
      auto sc = MultiTransformer::initialize();
      if ( !sc ) { return sc; }

      // Force debug messages
      // sc = setProperty( "OutputLevel", MSG::VERBOSE );

      if ( radiatorIsActive( Rich::Aerogel ) ) {
        error() << "Aerogel not supported in the 'RichFuture' framework" << endmsg;
        return StatusCode::FAILURE;
      }
      if ( !radiatorIsActive( Rich::Rich1Gas ) ) { _ri_debug << "Track segments for Rich1Gas are disabled" << endmsg; }
      if ( !radiatorIsActive( Rich::Rich2Gas ) ) { _ri_debug << "Track segments for Rich2Gas are disabled" << endmsg; }

      // derived condition data
      Detector::Rich1::addConditionDerivation( this );
      Detector::Rich2::addConditionDerivation( this );
      addConditionDerivation( {Detector::Rich1::DefaultConditionKey,                         // inputs
                               Detector::Rich2::DefaultConditionKey},                        //
                              inputLocation<SegMakerDetInfo>(),                              // output
                              [usedRads = radiatorIsActive()]( const Detector::Rich1& rich1, //
                                                               const Detector::Rich2& rich2 ) {
                                return SegMakerDetInfo{rich1, rich2, usedRads};
                              } );

      _ri_verbo << m_minP << " " << m_minPt << endmsg;

      m_randZeroOne = Rndm::Numbers( randSvc(), Rndm::Flat( 0.0, 1.0 ) );

      if ( m_ghostFrac > 0.0 ) { info() << "Will create " << 100.0 * m_ghostFrac << "% ghost tracks" << endmsg; }

      // return
      return sc;
    }

  public:
    /// Algorithm execution via transform
    OutData operator()( LHCb::MCParticles const&  mcParts,      //
                        LHCb::MCRichTracks const& mcRichtracks, //
                        LHCb::MCProperty const&   mcProps,      //
                        SegMakerDetInfo const&    detInfo,      //
                        DetectorElement const&    geometry ) const override {

      _ri_debug << "Found " << mcRichtracks.size() << " MCRichTracks" << endmsg;

      // container to return
      OutData data;
      // shortcuts to tuple contents
      auto& segments    = std::get<LHCb::RichTrackSegment::Vector>( data );
      auto& tkToSegsRel = std::get<Relations::TrackToSegments::Vector>( data );
      auto& segToTkRel  = std::get<Relations::SegmentToTrackVector>( data );
      auto& tks         = std::get<LHCb::Tracks>( data );
      auto& tkLinks     = std::get<LHCb::LinksByKey>( data );

      LHCb::Tracks::size_type tkIndex( 0 );

      // cache info for Track->MCParticle relations
      using TkToMCPs = std::unordered_map<const LHCb::MCParticle*, std::vector<const LHCb::Track*>>;
      TkToMCPs tkToMCPs;

      // For reconstructibility check
      MCTrackInfo trackInfo( mcProps );

      // create map from MCP -> MCRichTrack
      std::unordered_map<const LHCb::MCParticle*, const LHCb::MCRichTrack*> mcpToMCRT;
      for ( const auto mcRichTk : mcRichtracks ) {
        const auto mcp = ( mcRichTk ? mcRichTk->mcParticle() : nullptr );
        if ( mcp ) { mcpToMCRT[mcp] = mcRichTk; }
      }

      // Loop over MCParticles
      for ( const auto mcp : mcParts ) {

        // sanity check
        if ( !mcp ) { continue; }

        // get charge
        const auto charge = mcp->particleID().threeCharge() / 3;
        if ( 1 != abs( charge ) ) { continue; } // for some reason sometimes happens ...

        // Check we have an origin vertex
        const auto mcp_orig_pv = mcp->primaryVertex();
        if ( !mcp_orig_pv ) { continue; }
        _ri_verbo << "Origin Primary Vertex " << mcp_orig_pv->position() << endmsg;

        // Check origin vertex position
        const auto mcp_orig_v = mcp->originVertex();
        if ( !mcp_orig_v ) { continue; }
        _ri_verbo << "Origin Vertex " << mcp_orig_v->position() << endmsg;
        if ( m_tkOriginTol[0] < fabs( mcp_orig_v->position().X() ) || //
             m_tkOriginTol[1] < fabs( mcp_orig_v->position().Y() ) || //
             m_tkOriginTol[2] < fabs( mcp_orig_v->position().Z() ) ) {
          _ri_verbo << " -> Out of range -> rejected" << endmsg;
          continue;
        }

        // Check MCParticle momentum vector
        const auto mcp_vect = mcp->momentum().Vect();
        if ( mcp->momentum().Pt() < m_minPt ) { continue; }

        // Check MCParticle is reconstructible as a Long track
        if ( LHCb::Pr::Checker::reconstructibleType( mcp, LHCb::Pr::Checker::RecAs::isNotLong, trackInfo ).value() ) {
          _ri_verbo << " -> MCParticle is not reconstructible as Long -> rejected" << endmsg;
          continue;
        }

        // emulate tracking inefficiency
        if ( m_tkEff < 1.0 && m_randZeroOne.shoot() > m_tkEff ) { continue; }

        // Create an emulated 'reco' track
        auto tk = std::make_unique<LHCb::Track>();

        // lambda func to create track states
        auto makeState = [&]( const auto pnt, const auto mom, const LHCb::State::Location loc ) {
          const auto        x    = pnt.x();
          const auto        y    = pnt.y();
          const auto        z    = pnt.z();
          const auto        tx   = ( mom.z() > 0 ? mom.x() / mom.z() : 0.0 );
          const auto        ty   = ( mom.z() > 0 ? mom.y() / mom.z() : 0.0 );
          const auto        p2   = mom.mag2();
          const auto        qOvP = ( p2 > 0 ? charge / std::sqrt( p2 ) : 999 );
          const LHCb::State state( Gaudi::TrackVector{x, y, tx, ty, qOvP}, z, loc );
          _ri_verbo << "Created State : " << state << endmsg;
          tk->addToStates( std::move( state ) );
        };

        // Create 'first' state from origin vertex
        makeState( mcp_orig_v->position(), mcp_vect, LHCb::State::Location::FirstMeasurement );

        // Fake some parameters
        tk->setNDoF( 10 ); // ??
        tk->setChi2PerDoF( 1 );
        tk->setGhostProbability( 0 );
        tk->setLikelihood( 1 ); // ??
        tk->setHistory( LHCb::Track::History::TrackIdealPR );
        tk->setType( LHCb::Track::Types::Long );

        // Track errors... ??
        const LHCb::RichTrackSegment::StateErrors stateErrs{};

        // temporary container for segment indices
        Relations::SegmentIndices segList;

        // Get MC Rich Track if available
        const auto               findMCR  = mcpToMCRT.find( mcp );
        const LHCb::MCRichTrack* mcRichTk = ( findMCR != mcpToMCRT.end() ? findMCR->second : nullptr );

        // Check required RICH segments
        if ( mcRichTk ) {
          for ( const auto radiator : detInfo.radiators ) {
            const auto rad = radiator->radiatorID();
            if ( m_reqSegs[rad] && !mcRichTk->segmentInRad( rad ) ) {
              _ri_verbo << " -> Has no " << rad << " segment -> rejected" << endmsg;
              continue;
            }
          }
        }

        // Should we use this track to create a fake ghost track ?
        const bool asGhost = ( m_randZeroOne.shoot() <= m_ghostFrac );
        // Flip track parameters in X and/or Y (must be at least one)
        const bool ghostXflip = ( asGhost && m_randZeroOne.shoot() < 0.5 );
        const bool ghostYflip = ( asGhost && ( !ghostXflip || m_randZeroOne.shoot() < 0.5 ) );

        // Loop over all radiators
        for ( const auto radiator : detInfo.radiators ) {

          // which radiator
          const auto rad = radiator->radiatorID();
          _ri_verbo << " -> Considering radiator " << rad << endmsg;

          // State info to extract
          Gaudi::XYZPoint  entryPoint, exitPoint, midPoint;
          Gaudi::XYZVector entryStateMomentum, exitStateMomentum, midStateMomentum;

          // If we have a RichMCSegment use this
          if ( mcRichTk ) {
            // Use extended RICH MC information to form segment data
            _ri_verbo << "Building segments using RICH extended MC information" << endmsg;

            // Get MC segment for this track and radiator
            const auto mcSeg = mcRichTk->segmentInRad( rad );
            if ( !mcSeg ) { continue; }

            _ri_verbo << "  -> Found MCRichSegment " << mcSeg->key() << endmsg;
            _ri_verbo << "  -> " << *mcSeg << endmsg;

            // Apply selection cuts
            if ( mcSeg->pathLength() < m_minPathL[rad] ) { continue; }
            if ( mcSeg->mcRichOpticalPhotons().size() < m_minPhots[rad] ) { continue; }

            // Have an MC segment so make a reco segment

            // Get state information
            entryPoint         = mcSeg->entryPoint();
            entryStateMomentum = mcSeg->entryMomentum();
            exitPoint          = mcSeg->exitPoint();
            exitStateMomentum  = mcSeg->exitMomentum();
            midPoint           = mcSeg->bestPoint( 0.5 );
            midStateMomentum   = mcSeg->bestMomentum( 0.5 );

          } else if ( m_useIdealStates[rad] ) {
            //  Try and use ideal state creator tool ...
            _ri_verbo << "Building segments using ideal state creator" << endmsg;

            // radiator entry and exit z positions
            const auto zStart = ( Rich::Rich2Gas == rad ? m_nomZstates[2] : m_nomZstates[0] );
            const auto zEnd   = ( Rich::Rich2Gas == rad ? m_nomZstates[3] : m_nomZstates[1] );
            const auto zMid   = ( zStart + zEnd ) * 0.5;
            // form state vectors
            LHCb::StateVector entrySV, exitSV, midSV;
            const auto        sc = //
                ( m_ideal_state_creator->createStateVector( mcp, zStart, entrySV, geometry ) &&
                  m_ideal_state_creator->createStateVector( mcp, zEnd, exitSV, geometry ) &&
                  m_ideal_state_creator->createStateVector( mcp, zMid, midSV, geometry ) );
            if ( !sc ) { continue; }

            // Get the state info
            entryPoint         = entrySV.position();
            entryStateMomentum = entrySV.momentum();
            exitPoint          = exitSV.position();
            exitStateMomentum  = exitSV.momentum();
            midPoint           = midSV.position();
            midStateMomentum   = midSV.momentum();

            // Apply selection cuts
            const auto plength = std::sqrt( ( entryPoint - exitPoint ).Mag2() );
            if ( plength < m_minPathL[rad] ) { continue; }

          } else {
            // reject
            continue;
          }

          // Check point (x,y) boundaries
          auto checkBoundaries = [&]( const auto& point ) {
            const auto R2 = ( point.x() * point.x() + point.y() * point.y() );
            return ( fabs( point.x() ) < m_maxX[rad] && //
                     fabs( point.y() ) < m_maxY[rad] && //
                     ( m_minR2[rad] < 0.01 || R2 > m_minR2[rad] ) );
          };
          if ( !checkBoundaries( entryPoint ) || !checkBoundaries( midPoint ) || !checkBoundaries( exitPoint ) ) {
            continue;
          }

          // Sanity checks on entry/exit points
          if ( entryPoint.Z() < m_minZ[rad] || midPoint.Z() < m_minZ[rad] || exitPoint.Z() < m_minZ[rad] ) { continue; }

          // check min/max momentum on segment
          const auto entP = std::sqrt( entryStateMomentum.mag2() );
          const auto extP = std::sqrt( exitStateMomentum.mag2() );
          const auto midP = std::sqrt( midStateMomentum.mag2() );
          if ( entP < m_minP[rad] || midP < m_minP[rad] || extP < m_minP[rad] || //
               entP > m_maxP[rad] || midP > m_maxP[rad] || extP > m_maxP[rad] ) {
            continue;
          }

          // If we are using this track as a ghost, flip the data in X and/or Y
          // to break any correlations with RICH hits.
          if ( asGhost ) {
            if ( ghostXflip ) {
              entryStateMomentum.SetX( -entryStateMomentum.X() );
              exitStateMomentum.SetX( -exitStateMomentum.X() );
              midStateMomentum.SetX( -midStateMomentum.X() );
              entryPoint.SetX( -entryPoint.X() );
              exitPoint.SetX( -exitPoint.X() );
              midPoint.SetX( -midPoint.X() );
            }
            if ( ghostYflip ) {
              entryStateMomentum.SetY( -entryStateMomentum.Y() );
              exitStateMomentum.SetY( -exitStateMomentum.Y() );
              midStateMomentum.SetY( -midStateMomentum.Y() );
              entryPoint.SetY( -entryPoint.Y() );
              exitPoint.SetY( -exitPoint.Y() );
              midPoint.SetY( -midPoint.Y() );
            }
          }

          _ri_verbo << "   -> Passed selection cuts " << endmsg;
          _ri_verbo << "    -> Rad Points  | entry=" << entryPoint << " mid=" << midPoint << " exit=" << exitPoint
                    << endmsg;
          _ri_verbo << "    -> Rad Momenta | entry=" << entryStateMomentum << " mid=" << midStateMomentum
                    << " exit=" << exitStateMomentum << endmsg;

          // if get here segment will be saved so save relations
          segList.push_back( segments.size() ); // this gives the index for the next entry ...
          segToTkRel.push_back( tkIndex );

          // make track states for this radiator
          makeState( entryPoint, entryStateMomentum,
                     ( Rich::Rich1Gas == rad ? LHCb::State::Location::BegRich1 : LHCb::State::Location::BegRich2 ) );
          makeState( exitPoint, exitStateMomentum,
                     ( Rich::Rich1Gas == rad ? LHCb::State::Location::EndRich1 : LHCb::State::Location::EndRich2 ) );

          // Create intersection info
          Rich::RadIntersection::Vector intersects;
          intersects.emplace_back( entryPoint, entryStateMomentum, exitPoint, exitStateMomentum, radiator );

          // Finally create a segment in the container
          segments.emplace_back( std::move( intersects ), midPoint, midStateMomentum, //
                                 rad, radiator->rich(),                               //
                                 stateErrs, stateErrs, stateErrs,                     //
                                 mcp_orig_pv->position4vector() );

          // Set mean photon energy
          segments.back().setAvPhotonEnergy( richPartProps()->meanPhotonEnergy( rad ) );

          _ri_verbo << "Created RichTrackSegment : " << segments.back() << endmsg;
        }

        // Finally save the track
        if ( m_saveAllTracks || !segList.empty() ) {
          // Just use index as key ...
          const auto tkKey = tkIndex;
          // relations stuff...
          tkToSegsRel.emplace_back( tkKey, tkIndex );
          auto& tkRels          = tkToSegsRel.back();
          tkRels.segmentIndices = std::move( segList );
          // fill Track <-> MCParticle link table (if not being made as a ghost)
          if ( !asGhost ) { tkToMCPs[mcp].push_back( tk.get() ); }
          // Finally pass ownership to container
          _ri_verbo << "Created Track : " << *tk << endmsg;
          tks.insert( tk.release(), tkKey );
          // finally increment tk index last for next one
          ++tkIndex;
        }

      } // MCParticle loop

      // initialise the linker
      tkLinks = LHCb::LinksByKey{std::in_place_type<LHCb::Track>, std::in_place_type<LHCb::MCParticle>,
                                 LHCb::LinksByKey::Order::decreasingWeight};
      for ( const auto& [mcp, tks] : tkToMCPs ) {
        if ( !tks.empty() ) {
          const double weight = 1.0 / tks.size();
          for ( const auto tk : tks ) {
            _ri_verbo << "Creating MCP->TK link Tk=" << tk->key() << " MCP=" << mcp->key() << " weight=" << weight
                      << endmsg;
            tkLinks.link( tk->key(), mcp, weight );
          }
        }
      }

      // return the final data
      _ri_debug << "Created " << segments.size() << " track segments" << endmsg;
      return data;
    }

  private:
    // data

    /// random number between 0 and 1
    Rndm::Numbers m_randZeroOne{};

    /// Ideal sate creator
    ToolHandle<IIdealStateCreator> m_ideal_state_creator{this, "IdealStateCreator", "IdealStateCreator"};

  private:
    // properties

    /// Min path length for each radiator
    Gaudi::Property<RadiatorArray<double>> m_minPathL{
        this, "MinPathLengths", {10 * Gaudi::Units::mm, 500 * Gaudi::Units::mm, 1500 * Gaudi::Units::mm}};

    /// Min number of photons for each radiator
    Gaudi::Property<RadiatorArray<double>> m_minPhots{this, "MinNumPhotons", {0, 0, 0}};

    /// Overall Min monentum cut
    Gaudi::Property<RadiatorArray<double>> m_minP{
        this,
        "MinP",
        {0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV},
        "Minimum momentum (GeV/c)"};

    /// Overall Max monentum cut
    Gaudi::Property<RadiatorArray<double>> m_maxP{
        this,
        "MaxP",
        {9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV},
        "Maximum momentum (GeV/c)"};

    /// Minimum track transerve momentum
    Gaudi::Property<double> m_minPt{this, "MinPt", 0.0 * Gaudi::Units::GeV, "Minimum transerve momentum (GeV/c)"};

    /// Minimum z position for states in each radiator (mm)
    Gaudi::Property<RadiatorArray<double>> m_minZ{this, "MinStateZ", {800, 800, 9000}};

    /// Maximum X for states in each radiator
    Gaudi::Property<RadiatorArray<double>> m_maxX{this, "MaxX", {375, 500, 3000}};

    /// Maximum Y for states in each radiator
    Gaudi::Property<RadiatorArray<double>> m_maxY{this, "MaxY", {375, 500, 2500}};

    /// Minimum R^2 for states in each radiator
    Gaudi::Property<RadiatorArray<double>> m_minR2{this, "MinR2", {50 * 50, 50 * 50, 100 * 100}};

    /// Tolerance on track origin w.r.t. (0,0,0)
    Gaudi::Property<std::array<double, 3>> m_tkOriginTol{this, "TrackOriginTol", {1.0, 1.0, 100.0}};

    /// Require segments in given radiator
    Gaudi::Property<RadiatorArray<bool>> m_reqSegs{this, "RequireSegment", {false, false, false}};

    /// Tracking efficiency
    Gaudi::Property<double> m_tkEff{this, "TrackingEfficiency", 1.1, "Tracking efficiency to emulate"};

    /// Allow use of ideal state creator when RICH data is missing
    Gaudi::Property<RadiatorArray<bool>> m_useIdealStates{this, "UseIdealStates", {true, true, true}};

    /// Fraction of tracks to use to make 'ghosts'
    Gaudi::Property<double> m_ghostFrac{this, "GhostFraction", 0.0};

    /// Nominal z positions of states at RICHes
    Gaudi::Property<std::array<double, 2 * Rich::NRiches>> m_nomZstates{
        this,
        "NominalStateZ",
        {
            990 * Gaudi::Units::mm,  ///< Place to look for Rich1 entry state
            2165 * Gaudi::Units::mm, ///< Place to look for Rich1 exit state
            9450 * Gaudi::Units::mm, ///< Place to look for Rich2 entry state
            11900 * Gaudi::Units::mm ///< Place to look for Rich2 exit state
        },
        "The z positions to look for state at the entry/exit of RICH1/RICH2."};

    /// Always save tracks even if no RICH segments
    Gaudi::Property<bool> m_saveAllTracks{this, "SaveAllTracks", true, "Save all tracks"};

  }; // namespace Rich::Future::Rec::MC

  //=============================================================================

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrSegMakerFromMCRichTracks )

} // namespace Rich::Future::Rec::MC
