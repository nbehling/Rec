/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Include
#include "TrackKernel/Track1DTabFunc.h"

// Create object
/**
 * @brief flattening function of the GhostProbability
 *
 * @return instance of function. Gives ownership! User has to delete!
 */

namespace {

  std::unique_ptr<Track::TabulatedFunction1D> Update_VeloTable() {

    // Initialisation (should do this just once, slow)
    return std::make_unique<Track::TabulatedFunction1D>( std::initializer_list<float>{
        0.,       0.542234, 0.71428,  0.760395, 0.784289, 0.797606, 0.806131, 0.81191,  0.817205, 0.821531, 0.824445,
        0.827623, 0.831718, 0.836088, 0.838852, 0.842891, 0.846108, 0.848955, 0.851168, 0.85459,  0.857795, 0.861054,
        0.864481, 0.866758, 0.868892, 0.872248, 0.874124, 0.876244, 0.878907, 0.882626, 0.885134, 0.888377, 0.891033,
        0.893183, 0.894774, 0.897363, 0.899804, 0.90266,  0.905441, 0.907681, 0.90978,  0.912786, 0.914392, 0.916593,
        0.918823, 0.921085, 0.922446, 0.924665, 0.925924, 0.927298, 0.928455, 0.930399, 0.93134,  0.932348, 0.933346,
        0.934334, 0.935107, 0.935955, 0.937032, 0.93788,  0.939209, 0.939835, 0.940615, 0.941333, 0.94215,  0.942828,
        0.943539, 0.944071, 0.944635, 0.945445, 0.945959, 0.946469, 0.947199, 0.947604, 0.947995, 0.948528, 0.94897,
        0.949445, 0.94994,  0.950547, 0.950984, 0.951351, 0.951818, 0.952198, 0.952629, 0.952967, 0.953352, 0.953755,
        0.954262, 0.95474,  0.955162, 0.955494, 0.955897, 0.956372, 0.956866, 0.957392, 0.957903, 0.958178, 0.958471,
        0.958918, 0.959299, 0.959781, 0.960122, 0.960387, 0.96066,  0.961193, 0.961554, 0.962017, 0.962411, 0.962778,
        0.963105, 0.963354, 0.963696, 0.964135, 0.9645,   0.964757, 0.965094, 0.9655,   0.965875, 0.966153, 0.966727,
        0.967097, 0.967343, 0.967645, 0.968144, 0.968784, 0.969246, 0.969575, 0.969911, 0.970197, 0.970535, 0.970895,
        0.971404, 0.971829, 0.972152, 0.972616, 0.972885, 0.973162, 0.973439, 0.973843, 0.974322, 0.974703, 0.974936,
        0.975273, 0.97553,  0.975945, 0.976242, 0.976603, 0.977001, 0.977397, 0.977747, 0.978079, 0.978607, 0.978929,
        0.979251, 0.979493, 0.97979,  0.980182, 0.980417, 0.980755, 0.981141, 0.98131,  0.981612, 0.981827, 0.98226,
        0.982504, 0.982846, 0.983256, 0.983521, 0.983833, 0.984072, 0.984448, 0.984722, 0.985263, 0.985743, 0.986092,
        0.986492, 0.986825, 0.987351, 0.987842, 0.988253, 0.988689, 0.989171, 0.989692, 0.990053, 0.990568, 0.99098,
        0.991596, 0.992171, 0.993201, 0.993786, 0.994877, 0.995628, 0.996394, 0.997257, 0.998672, 0.999014, 0.999377,
        0.999584, 0.999817, 1.} );
  }
} // namespace
