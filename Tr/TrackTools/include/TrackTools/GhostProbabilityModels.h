/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Track.h"
#include "GhostProbabilityFeatures.h"
#include "LHCbMath/VectorizedML/Evaluators.h"
#include "LHCbMath/VectorizedML/Sequence.h"

namespace LHCb::GhostProbability {

  namespace details {
    using namespace LHCb::VectorizedML::Layers;

    template <typename Feats>
    struct GhostModel {
      constexpr static int nNeurons = Feats::Size + 5;
      constexpr static int nOutput  = 1;

      struct L1 : Linear<Feats::Size, nNeurons> {};
      struct A1 : ReLU<nNeurons> {};
      struct L2 : Linear<nNeurons, nNeurons> {};
      struct A2 : ReLU<nNeurons> {};
      struct L3 : Linear<nNeurons, nOutput> {};
      struct A3 : Sigmoid<nOutput> {};
      struct TF : PiecewiseLinearTransform<nOutput, 200> {};

      struct MLP : LHCb::VectorizedML::Sequence<Feats::Size, nOutput, L1, A1, L2, A2, L3, A3, TF> {};
      struct Evaluator : LHCb::VectorizedML::EvaluatorOnRange<MLP, Feats> {};
    };

    namespace Feats {
      using namespace Features;

      template <typename... Fs>
      using F = LHCb::VectorizedML::Features<Fs...>;

      struct Long : F<Chi2, NDOF, LogPt, Eta, FitVeloChi2, FitTChi2, FitTNDOF, FitMatchChi2, nUTHits, nUTOutliers> {};
      struct Downstream : F<Chi2, NDOF, LogPt, Eta, FitTChi2, FitTNDOF, nUTHits, nUTOutliers> {};
      struct Upstream : F<Chi2, NDOF, LogPt, Eta, FitVeloChi2, nUTHits, nUTOutliers> {};
      struct Ttrack : F<Chi2, nFTHits, LogPt, Eta> {};
      struct Velo : F<Chi2, nVPHits, Eta> {};

      struct Long_noUT : F<Chi2, NDOF, LogPt, Eta, FitVeloChi2, FitTChi2, FitTNDOF, FitMatchChi2> {};
    } // namespace Feats
  }   // namespace details

  // without UT feature configurations (Ttrack and Velo have same features)
  struct Long_noUT : details::GhostModel<details::Feats::Long_noUT>::Evaluator {};

  // nominal feature configurations
  struct Long : details::GhostModel<details::Feats::Long>::Evaluator {};
  struct Downstream : details::GhostModel<details::Feats::Downstream>::Evaluator {};
  struct Upstream : details::GhostModel<details::Feats::Upstream>::Evaluator {};
  struct Ttrack : details::GhostModel<details::Feats::Ttrack>::Evaluator {};
  struct Velo : details::GhostModel<details::Feats::Velo>::Evaluator {};
} // namespace LHCb::GhostProbability
