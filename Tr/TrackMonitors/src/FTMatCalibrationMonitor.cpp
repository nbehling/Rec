/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"
#include "Event/FitNode.h"
#include "Event/Measurement.h"
#include "Event/PrFitNode.h"
#include "Event/PrKalmanFitResult.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "Event/TrackParameters.h"
#include "FTDet/DeFTDetector.h"
#include "FTDet/DeFTMat.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/LHCbID.h"
#include "LHCbAlgs/Consumer.h"
#include "Map.h"
#include "TrackKernel/TrackFunctors.h"
#include "TrackMonitorTupleBase.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <fmt/format.h>
#include <yaml-cpp/yaml.h>

template <typename TFitResult, typename TNode>
class FTMatCalibrationMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range& tracks, const DeFT& ),
                                       LHCb::DetDesc::usesBaseAndConditions<TrackMonitorTupleBase, DeFT>> {

public:
  FTMatCalibrationMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {KeyValue{"TracksInContainer", LHCb::TrackLocation::Default},
                   KeyValue{"FTDetectorLocation", DeFTDetectorLocation::Default}} ){};

  StatusCode initialize() override { return Consumer::initialize(); }

  StatusCode finalize() override {
    return Consumer::finalize().andThen( [&] {
      if ( m_produceYamlFile ) {
        YAML::Node yamlNode;

        // TODO: find a better way to do this

        enum STATIONS { T1 = 1, T2, T3 };
        enum LAYERS { X1, U, V, X2, nLayers };
        enum QUARTERS { Q0, Q1, Q2, Q3, nQuarters };
        enum MODULES { M0, M1, M2, M3, M4, M5 };
        enum MATS { Mat0, Mat1, Mat2, Mat3, nMats };

        std::array<std::string, 4> layerStrings{"X1", "U", "V", "X2"};

        // Loop over the stations, layers, quarters, modules and mats to make the names of the conditions as strings
        for ( int station : {T1, T2, T3} ) {
          for ( int layer : {X1, U, V, X2} ) {
            for ( int quarter : {Q0, Q1, Q2, Q3} ) {
              for ( int module = M0; module <= ( station == T3 ? M5 : M4 ); module++ ) {
                for ( int mat : {Mat0, Mat1, Mat2, Mat3} ) {
                  auto globalStationIdx = station - 1;
                  auto globalLayerIdx   = nLayers * globalStationIdx + layer;
                  auto globalQuarterIdx = nQuarters * globalLayerIdx + quarter;
                  auto globalModuleIdx  = station < 3 ? 5 * globalQuarterIdx + module
                                                     : 5 * globalQuarterIdx + nQuarters * layer + quarter + module;

                  // the local and global numbering can be different depending on the quarter
                  auto readoutFromCtoE =
                      layer % 2 == 0 ? ( quarter == 1 || quarter == 2 ) : ( quarter == 0 || quarter == 3 );

                  const auto globalMatNumber = LHCb::Detector::FTChannelID::MatID(
                      nMats * globalModuleIdx + ( readoutFromCtoE ? mat : nMats - 1 - mat ) ); //

                  std::string conditionName =
                      fmt::format( "matContractionT{station}{layer}Q{quarter}M{module}Mat{mat}",
                                   fmt::arg( "station", int( station ) ), fmt::arg( "layer", layerStrings[layer] ),
                                   fmt::arg( "quarter", int( quarter ) ), fmt::arg( "module", int( module ) ),
                                   fmt::arg( "mat", int( mat ) ) );

                  // when applying the conditions, the readout direction must be taken into account
                  auto normalReadoutDir =
                      layer % 2 == 0 ? ( quarter == 0 || quarter == 1 ) : ( quarter == 2 || quarter == 3 );
                  auto sign = normalReadoutDir ? 1 : -1;

                  // get the histogram for this mat
                  auto& matHisto = m_histograms_mat_wrt_current_cond.at( globalMatNumber );

                  yamlNode["matContraction"][conditionName] =
                      YAML::Load( "[]" ); // is there a better way to make it appear flow style?

                  // loop over the bins in the histogram (starts at 1)
                  for ( std::size_t i = 1; i < FTConstants::nChannels * FTConstants::nSiPM + 1; i++ ) {
                    // binValue gives { { long unsigned int nEntries, double sum } double sumOfSqs }
                    const float sumOfSqs = std::get<double>( matHisto.UnbiasedresidualPerChannel.binValue( i ) );
                    const float sum      = std::get<double>( std::get<std::tuple<long unsigned int, double>>(
                        matHisto.UnbiasedresidualPerChannel.binValue( i ) ) );
                    const auto  nEntries = std::get<long unsigned int>( std::get<std::tuple<long unsigned int, double>>(
                        matHisto.UnbiasedresidualPerChannel.binValue( i ) ) );
                    const float val      = sum / nEntries;
                    const auto  variance = sumOfSqs / nEntries - val * val;
                    const auto  error    = sqrt( variance );
                    yamlNode["matContraction"][conditionName].push_back(
                        ( nEntries > m_minEntries && val / error > m_significanceThreshold )
                            ? -1 * sign * val
                            : 0 ); // to do -- how to smooth etc?

                  } // end loop over channels
                }   // end loop over mats
              }     // end loop over modules
            }       // end loop over quarters
          }         // end loop over layers
        }           // end loop over stations
        std::ofstream fileOut( m_outputFileName );
        fileOut << yamlNode;
        fileOut.close();
      } // end if
    } );
  }

  void operator()( const LHCb::Track::Range& tracks, const DeFT& ftDet ) const override;

private:
  Gaudi::Property<bool>        m_produceYamlFile{this, "ProduceYamlFile", false};
  Gaudi::Property<std::string> m_outputFileName{this, "OutputFileName", "matContraction.yaml"};
  Gaudi::Property<float>       m_significanceThreshold{this, "SignificanceThreshold", -100.};
  Gaudi::Property<float>       m_minEntries{this, "MinEntriesPerChannel", -100.};

  void fillContainers( const LHCb::Track& track, const DeFT& ftDet ) const;

private:
  // histogram to look at the unbiased residual, considering the current calibration values
  struct DifferencePerChannelHistogram {
    mutable Gaudi::Accumulators::ProfileHistogram<1> UnbiasedresidualPerChannel;

    DifferencePerChannelHistogram( const FTMatCalibrationMonitor* owner, std::string const& mat )
        : UnbiasedresidualPerChannel{owner,
                                     "DeltaUnbiasedresidualPerChannel" + mat,
                                     "Difference in unbiased residual per Channel, " + mat,
                                     {512, 0, 512}} {}
  };

  // histogram to look at the unbiased residual, subtracting the current calibration values (i.e. residual wrt no
  // calibration)
  struct Ref0PerChannelHistogram {
    mutable Gaudi::Accumulators::ProfileHistogram<1> UnbiasedresidualPerChannel;

    Ref0PerChannelHistogram( const FTMatCalibrationMonitor* owner, std::string const& mat )
        : UnbiasedresidualPerChannel{owner,
                                     "Ref0UnbiasedresidualPerChannel" + mat,
                                     "Unbiased residual per Channel wrt 0, " + mat,
                                     {512, 0, 512}} {}
  };

  std::map<LHCb::Detector::FTChannelID::MatID, DifferencePerChannelHistogram> m_histograms_mat_wrt_current_cond =
      []( const FTMatCalibrationMonitor* parent ) {
        std::map<LHCb::Detector::FTChannelID::MatID, DifferencePerChannelHistogram> map;
        for ( size_t globalMatNumber = 0; globalMatNumber < LHCb::Detector::FT::nMatsTotal; globalMatNumber++ ) {
          // todo: make naming consistent with the condition file
          map.emplace( std::piecewise_construct,
                       std::forward_as_tuple( LHCb::Detector::FTChannelID::MatID( globalMatNumber ) ),
                       std::forward_as_tuple( parent, "GlobalMat" + std::to_string( globalMatNumber ) ) );
        }
        return map;
      }( this );

  std::map<LHCb::Detector::FTChannelID::MatID, Ref0PerChannelHistogram> m_histograms_mat_wrt0 =
      []( const FTMatCalibrationMonitor* parent ) {
        std::map<LHCb::Detector::FTChannelID::MatID, Ref0PerChannelHistogram> map;
        for ( size_t globalMatNumber = 0; globalMatNumber < LHCb::Detector::FT::nMatsTotal; globalMatNumber++ ) {
          // todo: make naming consistent with the condition file
          map.emplace( std::piecewise_construct,
                       std::forward_as_tuple( LHCb::Detector::FTChannelID::MatID( globalMatNumber ) ),
                       std::forward_as_tuple( parent, "GlobalMat" + std::to_string( globalMatNumber ) ) );
        }
        return map;
      }( this );
};

using FTMatCalibMonitor_PrKalman = FTMatCalibrationMonitor<LHCb::PrKalmanFitResult, LHCb::Pr::Tracks::Fit::Node>;
DECLARE_COMPONENT_WITH_ID( FTMatCalibMonitor_PrKalman, "FTMatCalibrationMonitor" );

template <typename TFitResult, typename TNode>
void FTMatCalibrationMonitor<TFitResult, TNode>::operator()( const LHCb::Track::Range& tracks,
                                                             const DeFT&               ftDet ) const {

  for ( const LHCb::Track* track : tracks ) {
    const auto type = track->type();
    if ( !LHCb::Event::Enum::Track::hasT( type ) ) { continue; }
    if ( track->checkFitStatus( LHCb::Track::FitStatus::Fitted ) ) { fillContainers( *track, ftDet ); }
  }
}

template <typename TFitResult, typename TNode>
void FTMatCalibrationMonitor<TFitResult, TNode>::fillContainers( const LHCb::Track& track, const DeFT& ftDet ) const {

  const auto fitResult = dynamic_cast<const TFitResult*>( track.fitResult() );
  if ( !fitResult ) {
    throw GaudiException( "Fit result is NULL - track was not fitted or uses wrong fit result type", name(),
                          StatusCode::FAILURE );
  }

  for ( const auto& node : nodes( *fitResult ) ) {
    if ( !node.hasMeasurement() ) continue;
    if ( node.isHitOnTrack() && node.isFT() ) {
      LHCb::LHCbID lhcbID = id( node );
      assert( lhcbID.isFT() );

      LHCb::Detector::FTChannelID chan         = lhcbID.ftID();
      const auto                  globalMatIdx = chan.localMatIdx() + FTConstants::nMats * chan.globalModuleIdx();

      // get the histograms for this mat
      auto& histos_mats_wrt_current_cond =
          m_histograms_mat_wrt_current_cond.at( LHCb::Detector::FTChannelID::MatID( globalMatIdx ) );
      auto& histos_mats_wrt0 = m_histograms_mat_wrt0.at( LHCb::Detector::FTChannelID::MatID( globalMatIdx ) );
      auto  ftMat            = ftDet.findMat( chan );

      // the readout direction has to be considered when subtracting the current calibration values
      auto normalReadoutDir = chan.localLayerIdx() % 2 == 0
                                  ? ( chan.localQuarterIdx() == 0 || chan.localQuarterIdx() == 1 )
                                  : ( chan.localQuarterIdx() == 2 || chan.localQuarterIdx() == 3 );
      auto sign = normalReadoutDir ? 1 : -1;

      // get the channel
      auto [trackChannel, trackChannelFraction] =
          ftMat->calculateChannelAndFrac( ftMat->toLocal( state( node ).position() ).x() );
      histos_mats_wrt_current_cond
          .UnbiasedresidualPerChannel[trackChannel.channel() + trackChannel.sipm() * FTConstants::nChannels] +=
          node.unbiasedResidual();
      // double negative
      histos_mats_wrt0
          .UnbiasedresidualPerChannel[trackChannel.channel() + trackChannel.sipm() * FTConstants::nChannels] +=
          node.unbiasedResidual() -
          sign * ftMat->getmatContractionParameterVector()[trackChannel.channel() +
                                                           trackChannel.sipm() * FTConstants::nChannels];
    }
  }
}