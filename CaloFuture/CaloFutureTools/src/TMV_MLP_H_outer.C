/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Class: ReadMLPHOuter

/* configuration options =====================================================

#GEN -*-*-*-*-*-*-*-*-*-*-*- general info -*-*-*-*-*-*-*-*-*-*-*-

Method         : MLP::MLP
TMVA Release   : 4.2.1         [262657]
ROOT Release   : 6.24/00       [399360]
Creator        : calvom
Date           : Thu Jul  1 15:58:11 2021
Host           : Linux centos7-docker 4.18.0-193.19.1.el8_2.x86_64 #1 SMP Mon Sep 14 14:37:00 UTC 2020 x86_64 x86_64
x86_64 GNU/Linux Training events: 676000 Analysis type  : [Classification]

#OPT -*-*-*-*-*-*-*-*-*-*-*-*- options -*-*-*-*-*-*-*-*-*-*-*-*-

# Set by User:
NCycles: "500" [Number of training cycles]
HiddenLayers: "N" [Specification of hidden layer architecture]
NeuronType: "tanh" [Neuron activation function type]
V: "False" [Verbose output (short form of "VerbosityLevel" below - overrides the latter one)]
VarTransform: "N" [List of variable transformations performed before training, e.g.,
"D_Background,P_Signal,G,N_AllClasses" for: "Decorrelation, PCA-transformation, Gaussianisation, Normalisation, each for
the given class of events ('Al$ H: "True" [Print method-specific help message] TestRate: "5" [Test for overtraining
performed at each #th epochs] UseRegulator: "False" [Use regulator to avoid over-training] # Default: RandomSeed: "1"
[Random seed for initial synapse weights (0 means unique seed for each run; default value '1')] EstimatorType: "CE" [MSE
(Mean Square Estimator) for Gaussian Likelihood or CE(Cross-Entropy) for Bernoulli Likelihood] NeuronInputType: "sum"
[Neuron input function type] VerbosityLevel: "Default" [Verbosity level] CreateMVAPdfs: "False" [Create PDFs for
classifier outputs (signal and background)] IgnoreNegWeightsInTraining: "False" [Events with negative weights are
ignored in the training (but are included for testing and performance evaluation)] TrainingMethod: "BP" [Train with
Back-Propagation (BP), BFGS Algorithm (BFGS), or Genetic Algorithm (GA - slower and worse)] LearningRate: "2.000000e-02"
[ANN learning rate parameter] DecayRate: "1.000000e-02" [Decay rate for learning parameter] EpochMonitoring: "False"
[Provide epoch-wise monitoring plots according to TestRate (caution: causes big ROOT output file!)] Sampling:
"1.000000e+00" [Only 'Sampling' (randomly selected) events are trained each epoch] SamplingEpoch: "1.000000e+00"
[Sampling is used for the first 'SamplingEpoch' epochs, afterwards, all events are taken for training]
SamplingImportance: "1.000000e+00" [ The sampling weights of events in epochs which successful (worse estimator than
before) are multiplied with SamplingImportance, else they are divided.] SamplingTraining: "True" [The training sample is
sampled] SamplingTesting: "False" [The testing sample is sampled] ResetStep: "50" [How often BFGS should reset history]
Tau: "3.000000e+00" [LineSearch "size step"]
BPMode: "sequential" [Back-propagation learning mode: sequential or batch]
BatchSize: "-1" [Batch size: number of events/batch, only set if in Batch Mode, -1 for BatchSize=number_of_events]
ConvergenceImprove: "1.000000e-30" [Minimum improvement which counts as improvement (<0 means automatic convergence
check is turned off)] ConvergenceTests: "-1" [Number of steps (without improvement) required for convergence (<0 means
automatic convergence check is turned off)] UpdateLimit: "10000" [Maximum times of regulator update] CalculateErrors:
"False" [Calculates inverse Hessian matrix at the end of the training to be able to calculate the uncertainties of an
MVA value] WeightRange: "1.000000e+00" [Take the events for the estimator calculations from small deviations from the
desired value to large deviations only over the weight range]
##


#VAR -*-*-*-*-*-*-*-*-*-*-*-* variables *-*-*-*-*-*-*-*-*-*-*-*-

NVar 9
fracE1                  fracE1                fracE1                  fracE1 'F'
[-0.225853681564,0.912901759148] fracE2                  fracE2                fracE2 fracE2
'F'    [-0.251114070415,0.621873795986] fracE3                  fracE3                fracE3
fracE3                                                    'F'    [-0.238714829087,0.921248316765] fracE4
fracE4               fracE4                 fracE4 'F'    [-0.245630010962,0.924712240696]
fracEseed                 fracEseed               fracEseed                 fracEseed 'F'
[0.0188593510538,1.71566569805] fracE6                 fracE6               fracE6 fracE6
'F'    [-0.252621412277,0.907746732235] fracE7                 fracE7               fracE7
fracE7                                                   'F'    [-0.230187982321,0.963424623013] fracE8
fracE8               fracE8                 fracE8 'F'    [-0.269369602203,0.644044220448]
fracE9                 fracE9               fracE9                 fracE9 'F'
[-0.252370625734,0.946566343307] NSpec 0

============================================================================ */

#include "Kernel/STLExtensions.h"
#include "Kernel/TMV_utils.h"
#include <array>
#include <cmath>
#include <string_view>

namespace Data::ReadMLPHOuter {
  namespace {
    constexpr auto ActivationFnc = []( double x ) {
      // fast hyperbolic tan approximation
      if ( x > 4.97 ) return 1.f;
      if ( x < -4.97 ) return -1.f;
      float x2 = x * x;
      float a  = x * ( 135135.0f + x2 * ( 17325.0f + x2 * ( 378.0f + x2 ) ) );
      float b  = 135135.0f + x2 * ( 62370.0f + x2 * ( 3150.0f + x2 * 28.0f ) );
      return a / b;
    };

    constexpr auto OutputActivationFnc = []( double x ) {
      // sigmoid
      return 1.0 / ( 1.0 + exp( -x ) );
    };

    // Normalization transformation
    constexpr auto fMin_1 =
        std::array{std::array{-0.191468730569, -0.251114070415, -0.238714829087, -0.245630010962, 0.0188593510538,
                              -0.236244127154, -0.230187982321, -0.223056018353, -0.252370625734},
                   std::array{-0.225853681564, -0.207380637527, -0.235106766224, -0.210186034441, 0.0235662292689,
                              -0.252621412277, -0.216596588492, -0.269369602203, -0.201922953129},
                   std::array{-0.225853681564, -0.251114070415, -0.238714829087, -0.245630010962, 0.0188593510538,
                              -0.252621412277, -0.230187982321, -0.269369602203, -0.252370625734}};

    constexpr auto fMax_1 =
        std::array{std::array{0.912901759148, 0.620135545731, 0.871809363365, 0.869298577309, 1.71378040314,
                              0.90743470192, 0.919755160809, 0.586659193039, 0.946566343307},
                   std::array{0.900194227695, 0.621873795986, 0.921248316765, 0.924712240696, 1.71566569805,
                              0.907746732235, 0.963424623013, 0.644044220448, 0.901920735836},
                   std::array{0.912901759148, 0.621873795986, 0.921248316765, 0.924712240696, 1.71566569805,
                              0.907746732235, 0.963424623013, 0.644044220448, 0.946566343307}};

    // weight matrix from layer 0 to 1
    constexpr auto fWeightMatrix0to1 = std::array{
        std::array{-2.63783779795429, 2.99681558216399, 0.765957201171226, -2.61379982435056, -3.76392975375205,
                   4.17737394841874, -2.07734295556257, -0.0331113386847928, 2.04902944512898, -0.0610409171083741},
        std::array{-0.425646551903259, -2.39483279461827, 2.62112450202232, -4.81749381830912, 3.75681606261778,
                   3.26424149075393, -1.6908088681585, 2.0948656726522, 1.92167308702087, 0.747174868229813},
        std::array{2.841939598538, -3.09185533541239, 2.07850453342508, 1.26036278588272, -6.87423034856875,
                   -0.150815684547615, 3.75189730790889, -1.81365626523678, 3.7426344432008, -1.17964965890902},
        std::array{0.517246753228809, -2.42783345680948, -1.14560994594797, -1.46394267698195, -1.74208389980193,
                   6.18739513556233, 2.27624104069633, -3.04833584953965, -1.50470425959014, 0.0458114966255308},
        std::array{-2.87779451442692, 4.95346168665955, -5.1297533484951, 8.25694086406459, 10.2995745873078,
                   8.14113578186165, -8.59743882437723, 5.68389551651562, -8.25372578501055, -3.35191231146711},
        std::array{-2.94347030563167, 1.34114347707877, -2.92062207322333, 1.10143350547321, 3.38507108868554,
                   1.28298850696391, 1.47020029562246, 1.47569767993437, -1.63786045930472, 0.285375581717407},
        std::array{0.754930703926905, -0.292139016522224, 0.525505525724506, 0.129776914184119, 2.96785170479151,
                   2.24158787747547, -4.46322200652617, -5.47324248925436, 0.506418809413807, -0.66555853855955},
        std::array{1.79078390219955, -0.839168518328465, 2.37885556227126, -4.80061249541309, 3.94010753236038,
                   -4.14544965545653, 0.648434819634493, 0.250632958231791, 1.83782585264795, -1.82784330022728},
        std::array{4.30704448120911, 3.50044959948664, 0.00508727095800918, -8.96869121422014, 4.57921562472627,
                   3.41839711022584, 2.1784426120291, -7.00769539307194, -2.91916099961862, -1.62860301231356}};
    // weight matrix from layer 1 to 2
    constexpr auto fWeightMatrix1to2 =
        std::array{-0.971668066248549, 1.28433158174873,  1.36659135825722,  -1.15805954334154, -0.563544866075,
                   1.55571296066712,   0.895663106485581, -1.88181550520302, 0.608970770569372, -2.41861919144114};

    // the training input variables
    constexpr auto validator =
        TMV::Utils::Validator{"ReadMLPHOuter", std::tuple{"fracE1", "fracE2", "fracE3", "fracE4", "fracEseed", "fracE6",
                                                          "fracE7", "fracE8", "fracE9"}};

    constexpr auto transformer = TMV::Utils::Transformer{fMin_1, fMax_1};
    constexpr auto l0To1       = TMV::Utils::Layer{fWeightMatrix0to1, ActivationFnc};
    constexpr auto l1To2       = TMV::Utils::Layer{fWeightMatrix1to2, OutputActivationFnc};
    constexpr auto MVA         = TMV::Utils::MVA{validator, transformer, 2, l0To1, l1To2};
  } // namespace
} // namespace Data::ReadMLPHOuter

struct ReadMLPHOuter final {

  // constructor
  ReadMLPHOuter( LHCb::span<const std::string_view, 9> theInputVars ) {
    Data::ReadMLPHOuter::MVA.validate( theInputVars );
  }

  // the classifier response
  // "inputValues" is a vector of input values in the same order as the
  // variables given to the constructor
  static constexpr double GetMvaValue( LHCb::span<const double, 9> input ) { return Data::ReadMLPHOuter::MVA( input ); }
};
