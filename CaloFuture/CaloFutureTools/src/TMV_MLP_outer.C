/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Class: ReadMLPOuter

/* configuration options =====================================================

#GEN -*-*-*-*-*-*-*-*-*-*-*- general info -*-*-*-*-*-*-*-*-*-*-*-

Method         : MLP::MLP
TMVA Release   : 4.2.1         [262657]
ROOT Release   : 6.24/00       [399360]
Creator        : calvom
Date           : Fri Jun 18 16:01:59 2021
Host           : Linux centos7-docker 4.18.0-193.19.1.el8_2.x86_64 #1 SMP Mon Sep 14 14:37:00 UTC 2020 x86_64 x86_64
x86_64 GNU/Linux Training events: 47000 Analysis type  : [Classification]


#OPT -*-*-*-*-*-*-*-*-*-*-*-*- options -*-*-*-*-*-*-*-*-*-*-*-*-

# Set by User:
NCycles: "600" [Number of training cycles]
HiddenLayers: "N" [Specification of hidden layer architecture]
NeuronType: "tanh" [Neuron activation function type]
V: "False" [Verbose output (short form of "VerbosityLevel" below - overrides the latter one)]
VarTransform: "N" [List of variable transformations performed before training, e.g.,
"D_Background,P_Signal,G,N_AllClasses" for: "Decorrelation, PCA-transformation, Gaussianisation, Normalisation, each for
the given class of events ('Al$ H: "True" [Print method-specific help message] TestRate: "5" [Test for overtraining
performed at each #th epochs] UseRegulator: "False" [Use regulator to avoid over-training] # Default: RandomSeed: "1"
[Random seed for initial synapse weights (0 means unique seed for each run; default value '1')] EstimatorType: "CE" [MSE
(Mean Square Estimator) for Gaussian Likelihood or CE(Cross-Entropy) for Bernoulli Likelihood] NeuronInputType: "sum"
[Neuron input function type] VerbosityLevel: "Default" [Verbosity level] CreateMVAPdfs: "False" [Create PDFs for
classifier outputs (signal and background)] IgnoreNegWeightsInTraining: "False" [Events with negative weights are
ignored in the training (but are included for testing and performance evaluation)] TrainingMethod: "BP" [Train with
Back-Propagation (BP), BFGS Algorithm (BFGS), or Genetic Algorithm (GA - slower and worse)] LearningRate: "2.000000e-02"
[ANN learning rate parameter] DecayRate: "1.000000e-02" [Decay rate for learning parameter] EpochMonitoring: "False"
[Provide epoch-wise monitoring plots according to TestRate (caution: causes big ROOT output file!)] Sampling:
"1.000000e+00" [Only 'Sampling' (randomly selected) events are trained each epoch] SamplingEpoch: "1.000000e+00"
[Sampling is used for the first 'SamplingEpoch' epochs, afterwards, all events are taken for training]
SamplingImportance: "1.000000e+00" [ The sampling weights of events in epochs which successful (worse estimator than
before) are multiplied with SamplingImportance, else they are divided.] SamplingTraining: "True" [The training sample is
sampled] SamplingTesting: "False" [The testing sample is sampled] ResetStep: "50" [How often BFGS should reset history]
Tau: "3.000000e+00" [LineSearch "size step"]
BPMode: "sequential" [Back-propagation learning mode: sequential or batch]
BatchSize: "-1" [Batch size: number of events/batch, only set if in Batch Mode, -1 for BatchSize=number_of_events]
ConvergenceImprove: "1.000000e-30" [Minimum improvement which counts as improvement (<0 means automatic convergence
check is turned off)] ConvergenceTests: "-1" [Number of steps (without improvement) required for convergence (<0 means
automatic convergence check is turned off)] UpdateLimit: "10000" [Maximum times of regulator update] CalculateErrors:
"False" [Calculates inverse Hessian matrix at the end of the training to be able to calculate the uncertainties of an
MVA value] WeightRange: "1.000000e+00" [Take the events for the estimator calculations from small deviations from the
desired value to large deviations only over the weight range]
##


#VAR -*-*-*-*-*-*-*-*-*-*-*-* variables *-*-*-*-*-*-*-*-*-*-*-*-

NVar 10
fracE1             fracE1           fracE1             fracE1 'F'
[-0.00585694145411,0.48633980751] fracE2             fracE2           fracE2
fracE2                                               'F'    [-0.00372967659496,0.486372023821]
fracE3             fracE3           fracE3             fracE3 'F'
[-0.00521682342514,0.500095963478] fracE4            fracE4          fracE4
fracE4                                              'F'    [-0.00510058039799,0.495141297579]
fracEseed            fracEseed          fracEseed            fracEseed 'F'
[0.244679421186,0.99722892046] fracE6            fracE6          fracE6
fracE6                                              'F'    [-0.00351497041993,0.489519536495]
fracE7            fracE7          fracE7            fracE7 'F'
[-0.00498923519626,0.504979848862] fracE8            fracE8          fracE8
fracE8                                              'F'    [-0.00360777252354,0.494223743677]
fracE9            fracE9          fracE9            fracE9 'F'
[-0.00504248123616,0.482421725988] Et                            Et                            Et Et 'F'
[2.00008296967,36.9764709473] NSpec 0

============================================================================ */

#include "Kernel/STLExtensions.h"
#include "Kernel/TMV_utils.h"
#include <array>
#include <cmath>
#include <string_view>

namespace Data::ReadMLPOuter {
  namespace {
    constexpr auto ActivationFnc = []( double x ) {
      // fast hyperbolic tan approximation
      if ( x > 4.97 ) return 1.f;
      if ( x < -4.97 ) return -1.f;
      float x2 = x * x;
      float a  = x * ( 135135.0f + x2 * ( 17325.0f + x2 * ( 378.0f + x2 ) ) );
      float b  = 135135.0f + x2 * ( 62370.0f + x2 * ( 3150.0f + x2 * 28.0f ) );
      return a / b;
    };
    constexpr auto OutputActivationFnc = []( double x ) {
      // sigmoid
      return 1.0 / ( 1.0 + exp( -x ) );
    };

    // Normalization transformation, initialisation
    constexpr auto fMin_1 = std::array{
        std::array{-0.00585694145411, -0.00372967659496, -0.00521682342514, -0.00510058039799, 0.265968024731,
                   -0.00351497041993, -0.00498923519626, -0.00360777252354, -0.00504248123616, 2.00008296967},
        std::array{-0.00364904268645, -0.00269618327729, -0.00332048162818, -0.00353046110831, 0.244679421186,
                   -0.00212266086601, -0.0036069448106, -0.00335578480735, -0.00387839134783, 2.00026464462},
        std::array{-0.00585694145411, -0.00372967659496, -0.00521682342514, -0.00510058039799, 0.244679421186,
                   -0.00351497041993, -0.00498923519626, -0.00360777252354, -0.00504248123616, 2.00008296967}};

    constexpr auto fMax_1 =
        std::array{std::array{0.383371472359, 0.486372023821, 0.383990854025, 0.495141297579, 0.99722892046,
                              0.489519536495, 0.362838953733, 0.494223743677, 0.362343728542, 36.9764709473},
                   std::array{0.48633980751, 0.484035283327, 0.500095963478, 0.488680481911, 0.992968201637,
                              0.485506862402, 0.504979848862, 0.483686566353, 0.482421725988, 31.7305660248},
                   std::array{0.48633980751, 0.486372023821, 0.500095963478, 0.495141297579, 0.99722892046,
                              0.489519536495, 0.504979848862, 0.494223743677, 0.482421725988, 36.9764709473}};
    // weight matrix from layer 0 to 1
    constexpr auto fWeightMatrix0to1 =
        std::array{std::array{-3.8023074475667, -1.1221214762377, 2.67729876341333, 3.73281566600319, 0.653128471562461,
                              -0.507632318067886, -4.58566203590406, -2.18588917797908, 3.52357464869377,
                              0.394131138876674, -3.36915873255655},
                   std::array{0.144871873064987, -1.30573987015623, -0.808705453839635, 1.33078532747442,
                              1.30157239583867, -1.33986086386954, -0.121501717001273, -2.02123666830181,
                              -0.34229132951064, -0.749747452379817, -5.09893141279735},
                   std::array{4.85318720357496, -9.6831640688174, 8.60258099100305, 5.03462111007123, 7.66989107009247,
                              -12.8397618626741, 5.50602512531977, 5.11155841642725, 1.05356665208532,
                              -0.105548081196178, -0.142715616330995},
                   std::array{1.04315981966232, 1.57126018786599, 2.29117394573692, 1.30010261956654, 4.80174643967462,
                              1.5889202730569, 1.41090786425427, 1.87271462821717, 2.2689617364759, -9.20615595947603,
                              1.33669664210807},
                   std::array{-6.11673830608888, -5.62018583324095, -0.998492931945713, -5.62715172056906,
                              -8.51920730409647, 7.25714118322435, -0.912902168830915, 10.4842629949413,
                              -7.57034718225539, -0.18653250735766, -0.364694695939555},
                   std::array{-4.87188938981142, 9.41289316436391, -4.70115505322563, -1.94808960354632,
                              -2.59647542547226, -1.4549817353981, 1.46331210557046, -0.192582981588764,
                              1.29297042770919, 0.0931093081199275, 2.69770364330379},
                   std::array{-0.883186423555733, 3.59601676776759, 4.43381365354318, -11.4011699617978,
                              5.37285950559134, 3.56821696277832, 5.94345436669425, -5.17248363247441, 1.16390234226787,
                              0.199026174785122, -4.12062206085009},
                   std::array{-8.07622741788328, 8.28425556045971, -2.18081166641194, 11.86476073119, -7.87697859318641,
                              -5.10117278897715, -0.551058852872578, -5.26397332832142, -6.14105205677438,
                              -0.361183752933385, 0.434423771017723},
                   std::array{-3.72758604829265, -1.50984046660433, -3.86110697161158, 6.3712304417838,
                              -2.46231613875371, 6.3608403433007, -3.48964756819854, -1.76454458842433,
                              -3.42102412075924, 0.457620967471817, -1.11699580679959},
                   std::array{-2.3786280714693, 0.622088505208794, 0.420784883593261, -0.905587165298832,
                              -1.86147586255429, -0.803069671101432, -4.788827594347, 6.70623709889904,
                              -3.74563992377071, 0.247211727674174, -1.47770754812125}};
    // weight matrix from layer 1 to 2
    constexpr auto fWeightMatrix1to2 = std::array{
        -1.1922550294412, 1.77138755819638,  2.29525278387093, 1.74723300155767, -2.58468015463216, 3.14714041521783,
        2.54811119021798, -2.28774566665374, 3.25751825574194, 3.74292993231659, 0.0196932497138272};

    // the training input variables
    constexpr auto validator =
        TMV::Utils::Validator{"ReadMLPOuter", std::tuple{"fracE1", "fracE2", "fracE3", "fracE4", "fracEseed", "fracE6",
                                                         "fracE7", "fracE8", "fracE9", "Et"}};

    // Normalization transformation
    constexpr auto transformer = TMV::Utils::Transformer{fMin_1, fMax_1};
    constexpr auto l0To1       = TMV::Utils::Layer{fWeightMatrix0to1, ActivationFnc};
    constexpr auto l1To2       = TMV::Utils::Layer{fWeightMatrix1to2, OutputActivationFnc};
    constexpr auto MVA         = TMV::Utils::MVA{validator, transformer, 2, l0To1, l1To2};
  } // namespace
} // namespace Data::ReadMLPOuter

struct ReadMLPOuter final {

  // constructor
  ReadMLPOuter( LHCb::span<const std::string_view, 10> theInputVars ) {
    Data::ReadMLPOuter::MVA.validate( theInputVars );
  }

  // the classifier response
  // "inputValues" is a vector of input values in the same order as the
  // variables given to the constructor
  static constexpr double GetMvaValue( LHCb::span<const double, 10> inputValues ) {
    return Data::ReadMLPOuter::MVA( inputValues );
  }
};
