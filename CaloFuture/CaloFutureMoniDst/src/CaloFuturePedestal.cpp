/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"
#include "Core/FloatComparison.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloDigits_v2.h"
#include "Event/ODIN.h"
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiAlg/IHistoTool.h"
#include "LHCbAlgs/Consumer.h"
#include "TH2.h"
#include "TProfile2D.h"
#include "fmt/format.h"

namespace LHCb {

  using Input = Event::Calo::Digits;

  class CaloFuturePedestal final
      : public Algorithm::Consumer<void( const Input&, const LHCb::ODIN&, const DeCalorimeter& ),
                                   LHCb::DetDesc::usesBaseAndConditions<CaloFutureMoniAlg, DeCalorimeter>> {
  public:
    StatusCode initialize() override;
    void       operator()( const Input&, const LHCb::ODIN&, const DeCalorimeter& ) const override;
    StatusCode finalize() override;

    CaloFuturePedestal( const std::string& name, ISvcLocator* pSvcLocator );

  private:
    Gaudi::Property<bool> m_spectrum{this, "Spectrum", false, "activate spectrum per channel histogramming"};
    // HISTO IDS / TITLES
    GaudiAlg::HistoID m_ADCprofile1ID    = GaudiAlg::HistoID( fmt::format( "{}_ADC_profile2D_subch1", detData() ) );
    std::string       m_ADCprofile1Title = fmt::format( "{} Mean of ADCs, Subchannel 1", detData() );
    GaudiAlg::HistoID m_calorms1ID       = GaudiAlg::HistoID( fmt::format( "{}_ADC_RMS_2D_subch1", detData() ) );
    std::string       m_calorms1Title    = fmt::format( "{} RMS of ADCs, Subchannel 1", detData() );
    GaudiAlg::HistoID m_ADCprofile2ID    = GaudiAlg::HistoID( fmt::format( "{}_ADC_profile2D_subch2", detData() ) );
    std::string       m_ADCprofile2Title = fmt::format( "{} Mean of ADCs, Subchannel 2", detData() );
    GaudiAlg::HistoID m_calorms2ID       = GaudiAlg::HistoID( fmt::format( "{}_ADC_RMS_2D_subch2", detData() ) );
    std::string       m_calorms2Title    = fmt::format( "{} RMS of ADCs, Subchannel 2", detData() );
  };

  DECLARE_COMPONENT_WITH_ID( CaloFuturePedestal, "CaloFuturePedestal" )

} // namespace LHCb

LHCb::CaloFuturePedestal::CaloFuturePedestal( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"Input", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( name )},
                 KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                 KeyValue{"Detector", LHCb::Calo::Utilities::DeCaloFutureLocation( name )}} ) {}

StatusCode LHCb::CaloFuturePedestal::initialize() {
  return Consumer::initialize().andThen( [&] {
    info() << " digits from " << inputLocation() << endmsg;
    bool old_m_profile = m_profile;
    m_profile          = true;
    bookCaloFuture2D( m_ADCprofile1ID, m_ADCprofile1Title, detData(), -1 );
    bookCaloFuture2D( m_ADCprofile2ID, m_ADCprofile2Title, detData(), -1 );
    m_profile = false;
    bookCaloFuture2D( m_calorms1ID, m_calorms1Title, detData(), -1 );
    bookCaloFuture2D( m_calorms2ID, m_calorms2Title, detData(), -1 );
    m_profile = old_m_profile;
  } );
}

void LHCb::CaloFuturePedestal::operator()( const Input& digits, const LHCb::ODIN& odin,
                                           const DeCalorimeter& calo ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << name() << " execute " << endmsg;
  if ( digits.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found empty container in " << inputLocation() << endmsg;
    return;
  }
  const auto bcID = odin.bunchId();
  for ( const auto& digit : digits ) {
    const auto id  = digit.cellID();
    const auto adc = digit.adc();
    if ( digit.adc() < m_adcFilter ) continue;
    if ( bcID % 2 == 0 ) {
      fillCaloFuture2D( m_ADCprofile1ID, id, adc, calo, m_ADCprofile1Title );
      if ( m_spectrum ) {
        plot1D( adc, fmt::format( "{}Cells/subch1/{}/{}_{}_adc", detData(), id.areaName(), id.row(), id.col() ),
                fmt::format( "{} channel : {}", detData(), Gaudi::Utils::toString( id ) ), m_adcMin, m_adcMax,
                m_adcBin );
      }
    } else {
      fillCaloFuture2D( m_ADCprofile2ID, id, adc, calo, m_ADCprofile2Title );
      if ( m_spectrum ) {
        plot1D( adc, fmt::format( "{}Cells/subch2/{}/{}_{}_adc", detData(), id.areaName(), id.row(), id.col() ),
                fmt::format( "{} channel : {}", detData(), Gaudi::Utils::toString( id ) ), m_adcMin, m_adcMax,
                m_adcBin );
      }
    }
  }
  return;
}

StatusCode LHCb::CaloFuturePedestal::finalize() {
  TProfile2D* m_profile_root_rms1   = Gaudi::Utils::Aida2ROOT::aida2root( profile2D( m_ADCprofile1ID ) );
  TProfile2D* m_profile_root_rms2   = Gaudi::Utils::Aida2ROOT::aida2root( profile2D( m_ADCprofile2ID ) );
  TH2D*       m_histo_root_calorms1 = Gaudi::Utils::Aida2ROOT::aida2root( histo2D( m_calorms1ID ) );
  TH2D*       m_histo_root_calorms2 = Gaudi::Utils::Aida2ROOT::aida2root( histo2D( m_calorms2ID ) );

  auto fillH_fromProf = [this]( TProfile2D* prof, TH2D* h, int nsubch ) {
    if ( prof && h ) {
      Int_t n = prof->GetNcells();
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Bins in profile of sub-channel " << nsubch << " : " << n << endmsg;
      for ( Int_t i = 1; i < n; i++ ) {
        if ( !essentiallyZero( prof->GetBinError( i ) ) ) {
          h->SetBinContent( i, prof->GetBinError( i ) * sqrt( prof->GetBinEntries( i ) ) );
          // it would only be GetBinError(i) if option 's' could be set to the profile but AIDA 2D profile
          // doesn't support it AFAIU
        }
      }
    }
  };
  fillH_fromProf( m_profile_root_rms1, m_histo_root_calorms1, 1 );
  fillH_fromProf( m_profile_root_rms2, m_histo_root_calorms2, 2 );

  return Consumer::finalize();
}
