/***************************************************************************** \
 * (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/RelationWeighted1D.h"

namespace {
  using WeightedRelationTable = LHCb::RelationWeighted1D<LHCb::Particle, LHCb::Particle, float>;
  using Transformer =
      LHCb::Algorithm::Transformer<WeightedRelationTable( const LHCb::Particle::Range&, const LHCb::Particle::Range& )>;

  unsigned int get_overlap_in_lhcb_ids( const std::vector<LHCb::LHCbID>& vec_ids_a,
                                        const std::vector<LHCb::LHCbID>& vec_ids_b ) {
    return std::count_if( vec_ids_a.begin(), vec_ids_a.end(), [&]( const auto& lhcbID ) {
      return ( std::find( vec_ids_b.begin(), vec_ids_b.end(), lhcbID ) != vec_ids_b.end() );
    } );
  }

  std::vector<LHCb::LHCbID> get_velo_ids( const LHCb::Track& track ) {
    std::vector<LHCb::LHCbID> outputVector;
    const auto&               lhcbIdsOnTrack = track.lhcbIDs();

    std::copy_if( lhcbIdsOnTrack.begin(), lhcbIdsOnTrack.end(), std::back_inserter( outputVector ),
                  []( const auto& id ) { return id.isVP(); } );

    return outputVector;
  }
} // namespace

/**
 * Creates a relation table with the fraction of overlap
 * in LHCbIDs in the upgrade VELO between one set of
 * particles, and another.
 *
 * Standard use-case is the matching of VELO / Upstream tracks
 * to long tracks, where the matching table is created with
 * the particles.
 *
 * @author Laurent Dufour
 * @author Guillaume Max Pietrzyk
 *
 **/
class VeloIDOverlapRelationTable : public Transformer {
public:
  VeloIDOverlapRelationTable( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc, {KeyValue{"MatchFrom", ""}, KeyValue{"MatchTo", ""}},
                     KeyValue{"OutputRelations", ""} ) {}

  WeightedRelationTable operator()( const LHCb::Particle::Range& match_from_particles,
                                    const LHCb::Particle::Range& match_to_particles ) const override {
    WeightedRelationTable outputRelTable;

    for ( const auto* iParticleFrom : match_from_particles ) {
      if ( !iParticleFrom || ( !iParticleFrom->isBasicParticle() ) ) {
        ++m_msg_match_from_error;
        continue;
      }

      if ( !iParticleFrom->proto() || ( !iParticleFrom->proto()->track() ) ) {
        ++m_msg_match_from_pp_or_track_not_exist;
        continue;
      }

      const LHCb::Track* ref_probe_track    = iParticleFrom->proto()->track();
      const auto         ref_probe_velo_ids = get_velo_ids( *ref_probe_track );

      for ( const auto* iParticleTo : match_to_particles ) {
        if ( !iParticleTo ) {
          ++m_msg_match_to_nullptr;
          continue;
        }

        if ( !iParticleTo->isBasicParticle() ) {
          ++m_msg_match_to_not_basic;
          continue;
        }

        if ( !iParticleTo->proto() || !( iParticleTo->proto()->track() ) ) continue;
        if ( ref_probe_track == iParticleTo->proto()->track() ) continue;

        const LHCb::Track* long_track    = iParticleTo->proto()->track();
        const auto         long_velo_ids = get_velo_ids( *long_track );

        // Get the number of common VELO ids between the probe track and the long track.
        const auto nMatch_with_ref_probe = get_overlap_in_lhcb_ids( ref_probe_velo_ids, long_velo_ids );
        float      frac_nMatch_velo      = (float)( nMatch_with_ref_probe ) / (float)( ref_probe_velo_ids.size() );
        // if ( nMatch_with_ref_probe > m_min_match_fraction ) {
        if ( nMatch_with_ref_probe > 0 ) {
          m_match += 1;
          outputRelTable.relate( iParticleFrom, iParticleTo, frac_nMatch_velo ).ignore(); // The fraction of common
                                                                                          // velo ids between the
                                                                                          // probe and the long
                                                                                          // particles is the weight.
        } else
          m_zeromatch += 1;
      }
    }

    m_inCount += match_to_particles.size();
    m_refCount += match_from_particles.size();

    // Sort the relation table, such that best matches appear first
    outputRelTable.i_sort();

    return outputRelTable;
  }

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_inCount{this, "Match To Particles"};
  mutable Gaudi::Accumulators::SummingCounter<> m_refCount{this, "Match From Particles"};
  mutable Gaudi::Accumulators::SummingCounter<> m_match{this, "Found matches"};
  mutable Gaudi::Accumulators::SummingCounter<> m_zeromatch{this, "Non-matches considered"};

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_match_from_error{
      this, "Match from particle does not exist or is not a basic particle"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_match_from_pp_or_track_not_exist{
      this, "Could not find proto particle, or proto does not contain track", 2};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_match_to_nullptr{this, "Match to particle is a nullptr"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_match_to_not_basic{
      this, "Match to particle is not a basic particle"};
  Gaudi::Property<float> m_min_match_fraction{this, "MinMatchFraction", 0.0f};
};

DECLARE_COMPONENT( VeloIDOverlapRelationTable )
