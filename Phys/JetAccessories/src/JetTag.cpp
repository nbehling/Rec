/*****************************************************************************\
 * * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

#include "LHCbAlgs/Transformer.h"

// Event.
#include "Event/Particle.h"

// vertex relations
#include "Kernel/IRelatedPVFinder.h"

using namespace std;
using namespace LHCb;

/*
Receives lists of jets and return only the jets that are tagged by the list of tags.
The tagging condition is DeltaR<ConeSizeTag (default: 0.5)
*/

class JetTag : public LHCb::Algorithm::Transformer<LHCb::Particles( const LHCb::Particles&, const LHCb::Particles&,
                                                                    const LHCb::RecVertices& )> {

public:
  /// Constructor.
  JetTag( const string& name, ISvcLocator* svc );

  // Main method
  LHCb::Particles operator()( const LHCb::Particles&, const LHCb::Particles&, const LHCb::RecVertices& ) const override;

private:
  /// Tool to RelatedPVFinder
  ToolHandle<IRelatedPVFinder> m_prtVtxTool = {
      this, "RelatedPVFinder",
      "GenericParticle2PVRelator__p2PVWithIPChi2_OfflineDistanceCalculatorName_/P2PVWithIPChi2"};
  Gaudi::Property<float> m_ConeSizeTag{this, "ConeSizeTag", 0.5, "Cone size to tag jets."};
  Gaudi::Property<bool>  m_UseFlightDirection{this, "UseFlightDirection", false,
                                             "Flag to use flight direction if tag is composite."};
};

DECLARE_COMPONENT( JetTag )

/// Constructor.
JetTag::JetTag( const string& name, ISvcLocator* svc )
    : Transformer( name, svc, {KeyValue{"Jets", ""}, KeyValue{"Tags", ""}, KeyValue{"PVLocation", ""}},
                   {"Output", ""} ) {}

LHCb::Particles JetTag::operator()( const LHCb::Particles& Jets, const LHCb::Particles& Tags,
                                    const LHCb::RecVertices& PrimaryVertices ) const {

  static int count = 0;
  ++count;
  debug() << "=============== New event: " << count << "  ================" << endmsg;

  LHCb::Particles prts;

  for ( auto& jet : Jets ) {
    auto jetphi = jet->momentum().Phi();
    auto jeteta = jet->momentum().Eta();
    for ( auto& tag : Tags ) {
      auto tagphi = tag->momentum().Phi();
      auto tageta = tag->momentum().Eta();

      auto vrt = m_prtVtxTool->relatedPV( tag, PrimaryVertices );
      if ( !tag->isBasicParticle() && m_UseFlightDirection && vrt ) {
        // if particle is composite and flag to use flight direction is true
        Gaudi::XYZVector vec = tag->endVertex()->position() - vrt->position();
        tagphi               = vec.Phi();
        tageta               = vec.Eta();
      }

      auto dphi = tagphi - jetphi;
      auto deta = tageta - jeteta;
      while ( dphi > M_PI ) dphi -= 2 * M_PI;
      while ( dphi <= -M_PI ) dphi += 2 * M_PI;
      if ( sqrt( dphi * dphi + deta * deta ) < m_ConeSizeTag ) {
        prts.insert( new LHCb::Particle( *jet ) );
        break;
      }
    }
  }

  debug() << "Number of output particles: " << prts.size() << endmsg;

  return prts;
}
