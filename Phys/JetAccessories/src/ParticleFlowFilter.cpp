/*****************************************************************************\
 * * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

#include "LHCbAlgs/Transformer.h"

// Event.
#include "Event/Particle.h"

#include "Kernel/JetUtils.h"

using namespace std;
using namespace LHCb;

/*

Receives two lists of particles: listA and listB. Removes any particle from listA that belongs to listB.

*/

class ParticleFlowFilter
    : public LHCb::Algorithm::Transformer<LHCb::Particles( const LHCb::Particles&, const LHCb::Particles& )> {

public:
  /// Constructor.
  ParticleFlowFilter( const string& name, ISvcLocator* svc );

  // Main method
  LHCb::Particles operator()( const LHCb::Particles&, const LHCb::Particles& ) const override;

private:
};

DECLARE_COMPONENT( ParticleFlowFilter )

/// Constructor.
ParticleFlowFilter::ParticleFlowFilter( const string& name, ISvcLocator* svc )
    : Transformer( name, svc, {KeyValue{"Inputs", ""}, KeyValue{"ParticlesToBan", ""}},
                   {"Output", "Phys/ParticleFlow/Particles"} ) {}

LHCb::Particles ParticleFlowFilter::operator()( const LHCb::Particles& Inputs,
                                                const LHCb::Particles& InputsToBan ) const {

  static int count = 0;
  ++count;
  debug() << "=============== New event: " << count << "  ================" << endmsg;

  std::set<const LHCb::ProtoParticle*> toban;
  for ( auto& itb : InputsToBan ) {
    if ( itb->isBasicParticle() )
      toban.insert( itb->proto() );
    else {
      auto dtrs = LHCb::JetAccessories::getBasics( itb );
      for ( auto dtr : dtrs ) toban.insert( dtr->proto() );
    }
  }

  LHCb::Particles prts;

  for ( auto& Input : Inputs ) {
    if ( Input->isBasicParticle() ) {
      auto ret = toban.insert( ( Input )->proto() );
      if ( ret.second ) prts.insert( Input );
    } else {
      auto dtrs     = LHCb::JetAccessories::getBasics( Input );
      bool isdauban = false;
      for ( auto dtr : dtrs ) {
        auto ret = toban.insert( ( dtr )->proto() );
        if ( !ret.second ) isdauban = true;
      }
      if ( !isdauban ) prts.insert( Input );
    }
  }

  debug() << "Number of output particles: " << prts.size() << endmsg;

  return prts;
}
