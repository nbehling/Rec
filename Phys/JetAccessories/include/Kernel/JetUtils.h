/*****************************************************************************\
 * * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/
#ifndef LHCB_JETACCESSORIES_UTILS_H
#define LHCB_JETACCESSORIES_UTILS_H 1

#include "Event/Particle.h"

namespace LHCb {
  namespace JetAccessories {
    inline LHCb::Particle::ConstVector getBasics( const LHCb::Particle* p ) {
      LHCb::Particle::ConstVector r;
      auto                        helper = [&r]( const LHCb::Particle* p, auto fn ) -> void {
        if ( p->isBasicParticle() && p->proto() ) r.push_back( p );
        for ( auto& d : p->daughters() ) fn( d, fn );
      };
      helper( p, helper );
      return r;
    }
  } // namespace JetAccessories
} // namespace LHCb
#endif // LHCB_JETACCESSORIES_UTILS_H 1
