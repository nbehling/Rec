/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"

#include "GaudiKernel/IAlgTool.h"

#include "DetDesc/IGeometryInfo.h"

/** Particle transporter interface.
 *
 *  @author Edgar de Oliveira
 *  @date   22/02/2002
 *
 *  @author P. Koppenburg
 *  @date   16/12/2004
 *
 *  @author J. Palacios
 *  @date   15/04/2008
 */
struct GAUDI_API IParticleTransporter : virtual public IAlgTool {

  DeclareInterfaceID( IParticleTransporter, 5, 0 );

  /// Transport a Particle to specified z position.
  virtual StatusCode transport( LHCb::Particle const*, double znew, LHCb::Particle& transParticle,
                                IGeometryInfo const& geometry ) const = 0;

  /// Transport and project a Particle to specified z position.
  virtual StatusCode transportAndProject( LHCb::Particle const*, double znew, LHCb::Particle& transParticle,
                                          IGeometryInfo const& geometry ) const = 0;
};
