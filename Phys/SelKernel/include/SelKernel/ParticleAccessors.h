/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "LHCbMath/MatVec.h"
#include "SelKernel/ParticleTraits.h"
/** @namespace Sel::get
 *  @brief     Free-standing helper functions for extracting physical variables
 *
 *  This namespace collects helper functions for operations such like "get the
 *  energy of this object" that can perform some compile-time type
 *  introspection and support a range of different argument types.
 */
namespace Sel::get {
  template <typename T>
  auto chi2PerDoF( T const& x ) {
    if constexpr ( type_traits::has_chi2PerDoF_v<T> ) {
      return x.chi2PerDoF();
    } else {
      return x.chi2() / x.nDoF();
    }
  }
} // namespace Sel::get
