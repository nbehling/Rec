/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/with_functors.h"
#include "Functors/with_output_tree.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "LHCbAlgs/Consumer.h"

namespace {
  struct VoidFunctor {
    using Signature                    = std::any();
    static constexpr auto PropertyName = "Variable";
  };
  using base_type  = with_functors<LHCb::Algorithm::Consumer<void()>, VoidFunctor>;
  using value_type = double;
} // namespace

/** @class EventVariableMonitor EventVariableMonitor.cpp
 */
struct EventVariableMonitor final : public base_type {
  EventVariableMonitor( const std::string& name, ISvcLocator* pSvcLocator ) : base_type{name, pSvcLocator, {}} {}

  void operator()() const override {
    auto const&                                               functor = getFunctor<VoidFunctor>();
    Functors::detail::with_output_tree::numeric_value_wrapper w( functor.rtype() );

    std::visit(
        [&]( auto& x ) {
          auto unwrapped = std::any_cast<std::decay_t<decltype( x )>>( std::invoke( functor ) );
          ++( *m_histogram )[LHCb::Utils::as_arithmetic( unwrapped )];
        },
        w.get() );
  }

  StatusCode initialize() override {

    auto sc = base_type::initialize();
    if ( sc.isFailure() ) return sc;
    m_histogram.emplace( this, m_histogram_name.value(), m_histodef.value().title(),
                         Gaudi::Accumulators::Axis<value_type>{static_cast<unsigned int>( m_histodef.value().bins() ),
                                                               m_histodef.value().lowEdge(),
                                                               m_histodef.value().highEdge()} );
    return sc;
  }

private:
  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};

private:
  // properties
  Gaudi::Property<Gaudi::Histo1DDef> m_histodef{
      this, "HistogramDef", {"DefaultTitle", 0., 1., 100}, "Histogram definition"};

  Gaudi::Property<std::string> m_histogram_name{this, "HistogramName", "Histogram", "Histogram name"};
  mutable std::optional<Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, value_type>>
      m_histogram;
};

DECLARE_COMPONENT_WITH_ID( EventVariableMonitor, "Monitor__EventVariable" )
