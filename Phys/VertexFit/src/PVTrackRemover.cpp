/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LoKi/PhysExtract.h"

#include "Kernel/IPVReFitter.h"
#include "TrackInterfaces/IPVOfflineTool.h"

#include "Event/RecVertex.h"
#include "Event/Track.h"

#include "GaudiAlg/GaudiTool.h"

/**
 *  Remove weighted contribution of tracks from an LHCb::Particle
 *  to a given LHCb::RecVertex.
 *  This class is <b>temporary</b> and for testing purposes. The reFit
 *  method makes no sense and is not implemented.
 *
 *  @author Juan Palacios
 *  @date   2010-12-07
 */
class PVTrackRemover : public GaudiTool, virtual public IPVReFitter {
public:
  PVTrackRemover( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode initialize() override;
  StatusCode reFit( LHCb::VertexBase* PV, IGeometryInfo const& geometry ) const override;
  StatusCode remove( LHCb::Particle const* particle, LHCb::VertexBase* referencePV,
                     IGeometryInfo const& geometry ) const override;

private:
  std::string     m_pvToolType;
  IPVOfflineTool* m_pvTool = nullptr;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( PVTrackRemover )

// Standard constructor, initializes variables
PVTrackRemover::PVTrackRemover( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ), m_pvToolType( "PVOfflineTool" ) {
  declareInterface<IPVReFitter>( this );
  declareProperty( "IPVOffline", m_pvToolType );
}

StatusCode PVTrackRemover::initialize() {
  m_pvTool = tool<IPVOfflineTool>( m_pvToolType );
  warning() << "PVTrackRemover is no longer maintained and thus deprecated." << endmsg;
  if ( 0 == m_pvTool ) { return Error( "Could not load IPVOfflineTool " + m_pvToolType ); }
  return StatusCode::SUCCESS;
}

StatusCode PVTrackRemover::reFit( LHCb::VertexBase*, IGeometryInfo const& ) const {
  return Error( "PVTrackRemover::reFit makes no sense!" );
}

StatusCode PVTrackRemover::remove( LHCb::Particle const* particle, LHCb::VertexBase* PV,
                                   IGeometryInfo const& geometry ) const {
  if ( 0 == PV ) return Error( "NULL input PV" );
  LHCb::RecVertex* outPV = dynamic_cast<LHCb::RecVertex*>( PV );
  if ( 0 == outPV ) return Error( "Could now cast VertexBase to RecVertex" );
  const LHCb::RecVertex           referencePV( *outPV );
  std::vector<const LHCb::Track*> tracks;
  LoKi::Extract::tracks( particle, std::back_inserter( tracks ) );
  m_pvTool->removeTracksAndRecalculatePV( &referencePV, tracks, *outPV, geometry ).ignore();
  return StatusCode::SUCCESS;
}
