###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""@module Functors
Mathematical functions for use in functor expressions.
"""
from Functors.common import top_level_namespace
from Functors.grammar import WrappedBoundFunctor


def log(functor):
    """Wrap the math::log functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.log(PT) > 1.0
        ('operator>( ::Functors::math::log( Functors::chain( ::Functors::Common::Rho_Coordinate{}, ::Functors::Track::ThreeMomentum{} ) ), 1.0f )', '( ::Functors::math::log( ( RHO_COORDINATE @ THREEMOMENTUM ) ) > 1.0 )')
    """
    return WrappedBoundFunctor(top_level_namespace + 'math::log', functor)


def similarity(vec, cov_matrix):
    """Wrap for "Similarity" functor. Given vector "vec" of length "n" and covariance matrix of size "nxn", similarity computes: vec.transpose() * cov_matrix * vec

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import THREEMOMENTUM, THREE_MOM_COV_MATRIX, P
        >>> fmath.similarity(THREEMOMENTUM/P, THREE_MOM_COV_MATRIX)
        ('::Functors::math::similarity( operator/( ::Functors::Track::ThreeMomentum{}, Functors::chain( ::Functors::Common::Magnitude{}, ::Functors::Track::ThreeMomentum{} ) ), ::Functors::Particle::threeMomCovMatrix{} )', '::Functors::math::similarity( ( THREEMOMENTUM / ( MAGNITUDE @ THREEMOMENTUM ) ), THREE_MOM_COV_MATRIX )')
    """
    return WrappedBoundFunctor(top_level_namespace + 'math::similarity', vec,
                               cov_matrix)


def sign(functor):
    """Wrap the math::sign functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.sign(PT) > 0
        ('operator>( ::Functors::math::sign( Functors::chain( ::Functors::Common::Rho_Coordinate{}, ::Functors::Track::ThreeMomentum{} ) ), std::integral_constant<int, 0>{} )', '( ::Functors::math::sign( ( RHO_COORDINATE @ THREEMOMENTUM ) ) > 0 )')
    """
    return WrappedBoundFunctor(top_level_namespace + 'math::sign', functor)


def in_range(min_val, functor, max_val):
    """Wrap the math::in_range functor.

    This is efficient and only evaluates the functor once.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import MASS
        >>> fmath.in_range(1500, MASS, 2500)
        ('::Functors::math::in_range( std::integral_constant<int, 1500>{}, ::Functors::Composite::Mass{}, std::integral_constant<int, 2500>{} )', '::Functors::math::in_range( 1500, MASS, 2500 )')
    """
    return WrappedBoundFunctor(top_level_namespace + 'math::in_range', min_val,
                               functor, max_val)


def test_bit(functor_a, functor_b):
    return WrappedBoundFunctor(top_level_namespace + 'math::test_bit',
                               functor_a, functor_b)
