###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test if functors defined in module Functors are tested in TestFunctors.cpp.
In case this test fails, please add a few lines of code in TestFunctors.cpp which use the new functor.
'''
import Functors
import os


def error(msg):
    print("ERROR", msg)


# just add everything which is not tested for now.
exceptions = [
    'Functors::Adapters::ConvertToPOD', 'Functors::Adapters::Maximum',
    'Functors::Adapters::Minimum', 'Functors::Adapters::SubCombination',
    'Functors::Combination::Charge',
    'Functors::Combination::CosAngleBetweenDecayProducts',
    'Functors::Combination::DistanceOfClosestApproach',
    'Functors::Combination::DistanceOfClosestApproachChi2',
    'Functors::Combination::MaxDistanceOfClosestApproach',
    'Functors::Combination::MaxDistanceOfClosestApproachChi2',
    'Functors::Combination::MaxDistanceOfClosestApproachChi2Cut',
    'Functors::Combination::MaxDistanceOfClosestApproachCut',
    'Functors::Combination::MaxSDistanceOfClosestApproach',
    'Functors::Combination::MaxSDistanceOfClosestApproachChi2',
    'Functors::Combination::MaxSDistanceOfClosestApproachChi2Cut',
    'Functors::Combination::MaxSDistanceOfClosestApproachCut',
    'Functors::Combination::SDistanceOfClosestApproach',
    'Functors::Combination::SDistanceOfClosestApproachChi2',
    'Functors::Composite::ComputeDecayLengthSignificance',
    'Functors::Composite::CosDirectionAngleToVertex',
    'Functors::Composite::DeltaRhoToVertex',
    'Functors::Composite::DeltaXToVertex',
    'Functors::Composite::DeltaYToVertex',
    'Functors::Composite::DeltaZToVertex', 'Functors::Composite::EndVertexRho',
    'Functors::Composite::FlightDirectionVectorToVertex',
    'Functors::Composite::FlightDistanceChi2ToVertex',
    'Functors::Composite::Lifetime', 'Functors::Composite::Mass',
    'Functors::Composite::MassWithHypotheses',
    'Functors::Composite::MotherTrajectoryDistanceOfClosestApproachChi2',
    'Functors::Composite::PseudoRapidityFromVertex',
    'Functors::Examples::GreaterThan', 'Functors::Examples::ThorBeatsLoki',
    'Functors::Examples::TimesTwo', 'Functors::MVA',
    'Functors::Track::ClosestToBeamState', 'Functors::Track::Covariance',
    'Functors::Track::FourMomentum',
    'Functors::Track::ImpactParameterChi2ToVertex',
    'Functors::Track::ImpactParameterToVertex', 'Functors::Track::PIDe',
    'Functors::Track::PIDk', 'Functors::Track::PIDmu', 'Functors::Track::PIDp',
    'Functors::Track::PIDpi', 'Functors::Track::Phi',
    'Functors::Track::QoverP', 'Functors::Track::ReferencePointX',
    'Functors::Track::ReferencePointY', 'Functors::Track::ReferencePointZ',
    'Functors::Track::Flag', 'Functors::Track::TX', 'Functors::Track::TY',
    'Functors::Track::history', 'Functors::Track::nDoF',
    'Functors::Track::nFTHits', 'Functors::Track::nUTHits',
    'Functors::Track::nVPHits', 'Functors::Track::nPRVelo3DExpect',
    'Functors::Common::ForwardArgs<>( ', 'Functors::Common::ForwardArgs<0>( ',
    'Functors::Common::ForwardArgs<1>( ', 'Functors::Common::ForwardArgs<2>( ',
    'Functors::Common::UnitVector', 'Functors::Common::TES',
    'Functors::Common::Call', 'Functors::Common::Dot',
    'Functors::Common::NormedDot', 'Functors::Common::BestPV',
    'Functors::Common::ToLinAlg', 'Functors::Common::Phi_Coordinate',
    'Functors::Examples::PlusN',
    'Functors::Common::ImpactParameterChi2ToVertex',
    'Functors::Common::ImpactParameterChi2', 'Functors::Functional::MapAllOf',
    'Functors::Decay::FindDecay', 'Functors::Decay::FindMCDecay',
    'Functors::Column_t', 'Functors::Identity_t',
    'Functors::Particle::GetProtoParticle',
    'Functors::Adapters::GenerationFromCompositeV1( /* The generation of the children. Generation 1 are direct children, Generation 2 are grandchildren and so on. */ std::integral_constant<int, 1>{}',
    'Functors::Adapters::GenerationFromCompositeV1( /* The generation of the children. Generation 1 are direct children, Generation 2 are grandchildren and so on. */ std::integral_constant<int, 2>{}',
    'Functors::Particle::ParticlePropertyUser',
    'Functors::PID::RichThresholdDe', 'Functors::PID::RichThresholdKa',
    'Functors::PID::RichThresholdMu', 'Functors::PID::RichThresholdPi',
    'Functors::PID::RichThresholdPr'
]

functor_cpp_names = []
for name, val in Functors.__dict__.items():
    if isinstance(val, Functors.grammar.ComposedBoundFunctor):
        # Do we need to check composed functors?
        # right now this isn't supported
        continue
    if isinstance(val, Functors.grammar.BoundFunctor):
        functor_cpp_names.append(val._code[2:-2])
    elif isinstance(val, Functors.grammar.FunctorBase):
        functor_cpp_names.append(val._cppname[2:])
functor_cpp_names = sorted(functor_cpp_names)

test_file_name = os.path.expandvars(
    '$FUNCTORCOREROOT/tests/src/TestFunctors.cpp')
tested_functors = []
with open(test_file_name) as test_file:
    file_content = test_file.read()
    if ("using namespace Functors" in file_content):
        print(
            "Warning: Using namespaces might break the heuristics of this test. Try not to use them."
        )
    for functor in functor_cpp_names:
        if functor in file_content:
            print(
                f"Functor {functor} is tested for some class, which actually?!"
            )
            tested_functors.append(functor)
        else:
            print(f"Functor {functor} is NOT tested!")

untested_functors = list(set(functor_cpp_names).difference(tested_functors))
n_untested_functors = len(untested_functors)

print()
print("Before ignoring exceptions")
print(
    f"{n_untested_functors} of {len(functor_cpp_names)} factors are not tested!"
)

untested_functors = list(set(untested_functors).difference(exceptions))
n_untested_functors = len(untested_functors)
print("After ignoring exceptions")
print(
    f"{n_untested_functors} of {len(functor_cpp_names)} factors are not tested!"
)

if n_untested_functors > 0:
    print(untested_functors)
    error(
        "Above functor(s) not compiled in TestFunctors.cpp. Please add a test."
    )
    exit(1)
