/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TaggingHelperTool.h"
#include "TaggingHelper.h"

#include <cmath>

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TaggingHelperTool )

std::optional<std::pair<double, double>> TaggingHelperTool::calcIPWithErr( const LHCb::Particle&   part,
                                                                           const LHCb::VertexBase& vertex,
                                                                           const IGeometryInfo&    geometry ) const {
  double     ip     = -100.0;
  double     ipChi2 = 0;
  StatusCode sc     = m_distCalcTool->distance( &part, &vertex, ip, ipChi2, geometry );
  if ( sc.isFailure() ) return std::nullopt;
  return std::make_pair( ip, ip / std::sqrt( ipChi2 ) );
}

std::optional<std::pair<double, double>> TaggingHelperTool::calcIPWithChi2( const LHCb::Particle&   part,
                                                                            const LHCb::VertexBase& vertex,
                                                                            const IGeometryInfo&    geometry ) const {
  double     ip     = -100.0;
  double     ipChi2 = 0;
  StatusCode sc     = m_distCalcTool->distance( &part, &vertex, ip, ipChi2, geometry );
  if ( sc.isFailure() ) return std::nullopt;
  return std::make_pair( ip, ipChi2 );
}

std::optional<std::pair<double, double>>
TaggingHelperTool::calcMinIPWithErr( const LHCb::Particle& part, LHCb::span<LHCb::VertexBase const* const> vertices,
                                     const IGeometryInfo& geometry ) const {
  return std::accumulate( vertices.begin(), vertices.end(), std::optional<std::pair<double, double>>{},
                          [&]( auto prev, const auto* vertex ) {
                            auto tempIP = calcIPWithErr( part, *vertex, geometry );
                            return ( !prev || ( tempIP && tempIP->first < prev->first ) ) ? tempIP : prev;
                          } );
}

std::optional<std::pair<double, double>>
TaggingHelperTool::calcMinIPWithChi2( const LHCb::Particle& part, LHCb::span<LHCb::VertexBase const* const> vertices,
                                      const IGeometryInfo& geometry ) const {
  return std::accumulate( vertices.begin(), vertices.end(), std::optional<std::pair<double, double>>{},
                          [&]( auto prev, const auto* vertex ) {
                            auto tempIP = calcIPWithChi2( part, *vertex, geometry );
                            return ( !prev || ( tempIP && tempIP->first < prev->first ) ) ? tempIP : prev;
                          } );
}

std::optional<std::pair<double, double>>
TaggingHelperTool::calcMinDocaWithErr( const LHCb::Particle& particle, const LHCb::Particle& referenceParticleA,
                                       const LHCb::Particle& referenceParticleB, const IGeometryInfo& geometry ) const {
  double docaA    = 0;
  double docaErrA = 0;
  double docaB    = 0;
  double docaErrB = 0;

  const StatusCode scA = m_distCalcTool->distance( &particle, &referenceParticleA, docaA, docaErrA, geometry );
  const StatusCode scB = m_distCalcTool->distance( &particle, &referenceParticleB, docaB, docaErrB, geometry );

  if ( scA && scB ) {
    return std::make_pair( std::min( docaA, docaB ), std::min( docaErrA, docaErrB ) );
  } else {
    return std::nullopt;
  }
}

bool TaggingHelperTool::passesCommonPreSelection( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                                  LHCb::span<LHCb::VertexBase const* const> pileups,
                                                  const IGeometryInfo&                      geometry ) const {
  if ( m_overlapTool->foundOverlap( &tagCand, &bCand ) ) return false;

  auto bDaughters = m_descendantsTool->descendants( &bCand );
  bDaughters.push_back( &bCand );

  double minPhi = std::numeric_limits<double>::max();
  for ( const auto& bDaughter : bDaughters ) {
    if ( TaggingHelper::cloneCategory( tagCand, *bDaughter, m_maxSharedHitFraction ) !=
         TaggingHelper::CloneStatus::DifferentParticles )
      return false;

    double tempPhi = fabs( TaggingHelper::dPhi( tagCand.momentum(), bDaughter->momentum() ) );
    if ( tempPhi < minPhi ) minPhi = tempPhi;
  }
  if ( minPhi < m_minDistPhi ) return false;

  if ( pileups.empty() ) return true;
  auto puMinIP = calcMinIPWithChi2( tagCand, pileups, geometry );
  if ( !puMinIP ) return false;
  if ( std::sqrt( puMinIP->second ) < m_minIpSigTagPileUpVertices ) return false;

  return true;
}

bool TaggingHelperTool::hasOverlap( const LHCb::Particle& tagCand, const LHCb::Particle& bCand ) const {
  return m_overlapTool->foundOverlap( &tagCand, &bCand );
}

std::optional<LHCb::Vertex> TaggingHelperTool::fitVertex( const LHCb::Particle& tagCand, const LHCb::Particle& bCand,
                                                          const IGeometryInfo& geometry ) const {
  LHCb::Vertex vtx;
  StatusCode   sc = m_vertexFitTool->fit( vtx, tagCand, bCand, geometry );
  if ( sc.isFailure() ) return std::nullopt;
  return vtx;
}
