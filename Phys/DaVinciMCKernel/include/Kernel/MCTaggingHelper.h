/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MCParticle.h"
#include <iostream>

namespace LHCb::FlavourTagging {

  enum struct originTagCodes {
    SignalB_Decay          = 0,
    SS_Fragmentation       = 1,
    OS_B_Decay             = 2,
    OS_ExcitedB_Decay      = 3,
    OS_b_Fragmentation     = 4,
    Prompt_From_SignalB_PV = 5,
    Other_From_SignalB_PV  = 6,
    Not_From_SignalB_PV    = 100
  };
  std::ostream& operator<<( std::ostream&, originTagCodes );

  originTagCodes originType( const LHCb::MCParticle& bMC, const LHCb::MCParticle& bAnMC, const LHCb::MCParticle& tMC );
  originTagCodes originType( const LHCb::MCParticle& bMC, const LHCb::MCParticle& tMC );

}; // namespace LHCb::FlavourTagging
