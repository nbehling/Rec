/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#pragma once

#include <GaudiKernel/Kernel.h>
#include <vector>

struct IDVAlgorithm;
namespace LHCb {
  class Particle;
}
// ===========================================================================
namespace DaVinci {
  // ==========================================================================
  /** @class PVSentry
   *  Helper class to guarantee the removal of relations to the
   *  temporary object
   *
   *  The class is moved from CombineParticles
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2009-05-05
   */
  class GAUDI_API PVSentry {
    // ========================================================================
  public:
    // ========================================================================
    /** constructor
     *  @param parent   the davinci algorithm that holds relation tables
     *  @param particle the particle
     */
    PVSentry( const IDVAlgorithm* parent, const LHCb::Particle* particle, const bool tree = false );
    /// destructor
    ~PVSentry();
    // ========================================================================
  private:
    // ========================================================================
    /// the parent
    const IDVAlgorithm* m_parent;
    /// the temporary particle
    std::vector<const LHCb::Particle*> m_particles;
    // ========================================================================
  };
  // ==========================================================================
} // namespace DaVinci
