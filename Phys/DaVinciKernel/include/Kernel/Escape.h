/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef DAVINCIKERNEL_ESCAPE_H
#  define DAVINCIKERNEL_ESCAPE_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#  include <string>
#  include <vector>
// ============================================================================
namespace Decays {
  // ==========================================================================
  /** Replace all special characters in a particle name
   *
   * @author J. Borel
   * @author P. Koppenburg : Move to DaVinciKernel
   *
   */
  std::string escape( const std::string& input );
  // ==========================================================================
} // end of namespace Decays
// ============================================================================
// The END
// ============================================================================
#endif // DAVINCIKERNEL_ESCAPE_H
// ============================================================================
