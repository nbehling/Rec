/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_PRKERNELDICT_H
#define DICT_PRKERNELDICT_H 1

#include "PrKernel/IPrDebugTool.h"
#include "PrKernel/IPrDebugTrackingTool.h"
#include "PrKernel/IPrDebugUTTool.h"
#include "PrKernel/IPrUTCounter.h"
#include "PrKernel/IPrUTMagnetTool.h"

#endif // DICT_PRKERNELDICT_H
