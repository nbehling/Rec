2023-03-22 Rec v35r6
===

This version uses
Lbcom [v34r6](../../../../Lbcom/-/tags/v34r6),
LHCb [v54r6](../../../../LHCb/-/tags/v54r6),
Detector [v1r10](../../../../Detector/-/tags/v1r10),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r5](/../../tags/v35r5), with the following changes:

### New features ~"new feature"

- ~RICH | RichSIMDPhotonCherenkovAngles - Add per PMT column resolution plots for H and R type PMTs, !3346 (@jonrob)
- ~Jets ~"MC checking" | Update JetAccessories to reconstruct MC jets, !3277 (@helder)
- Make TrackMuonMatching DD4Hep compatible, !3355 (@decianm)
- Add MuonPID functors, !3341 (@rvazquez)
- Sign functor, TILT functor, y-z straight-line intersection functors, !3274 (@isanders)
- Allow more deterministic comparison for Particle when used as 'from' in Relations, !3343 (@graven)
- SOACollection particlev2, !3247 (@ahennequ)


### Fixes ~"bug fix" ~workaround

- Explicitly sort merged relations table, !3339 (@graven) [Moore#538]
- Make ProtoParticle -> MCParticle association table deterministic, !3336 (@graven)
- ~Composites | Fix incorrect daughter assignment in ParticleVertexFit, !3352 (@jzhuo)
- Solve test crashes, !3350 (@lohenry)
- Bugfix, taking into account neutrals in the B final state, !3348 (@cprouve)


### Enhancements ~enhancement

- Velo: Add warning message if rawbank is empty, !3354 (@ahennequ)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | Replace ProbeAndLongMatcher with MuonProbeToLongMatcher, !3333 (@rowina)
- Remove spurious base class of `LHCb::Calo::ECorrection`, !3344 (@raaij) [GAUDI-1023]
- Replace LinkerTable with LinkedTo, !3329 (@graven)
- Avoid two (out of three) gcc12 compiler warnings in PrLongLivedTracking, !3342 (@graven)
- Fixed tests which depend on unique names, !3196 (@sponce)


### Documentation ~Documentation


### Other

