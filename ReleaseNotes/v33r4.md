2021-12-02 Rec v33r4
===

This version uses
Lbcom [v33r5](../../../../Lbcom/-/tags/v33r5),
LHCb [v53r5](../../../../LHCb/-/tags/v53r5),
Gaudi [v36r2](../../../../Gaudi/-/tags/v36r2) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Rec [v33r3](/../../tags/v33r3), with the following changes:

### New features ~"new feature"

- ~Tracking | PrKalmanFilter but as a Tool!, !2539 (@gunther)
- ~Tracking ~FT | Add FTTrackSelector, !2602 (@shollitt)
- ~Monitoring ~Tuples | Expert Downstream Track Monitoring Algorithm, !2365 (@ggolasze)
- ~Tuples | Add functor to access decaytreefitted particles, !2504 (@pkoppenb)


### Fixes ~"bug fix" ~workaround

- ~Calo | Replace deprecated ToolHandle constuctor in MergedPi0, !2606 (@chasse)
- ~Composites ~Functors | Add v1::Particle compatibility layer to MASSWITHHYPOTHESES Functor, !2583 (@agilman) [#220]
- ~Functors | Fix BPVDLS in thor, !2589 (@mengzhen)
- ~Functors | Fix ThOr LifeTime bug, !2566 (@agilman) [#204]
- ~Monitoring | Fix profile plots for VP, FT, UT hit monitors, !2623 (@valukash)


### Enhancements ~enhancement

- ~Tracking | New methods for PrFitNode + PrKalmanFilter as library and module, !2603 (@jkubat)
- ~Tracking ~"Event model" | V3::Tracks compatible extrapolators, !2580 (@ausachov) [#203]
- ~Calo ~Monitoring | Calo monitoring Time Alignment and 2D histograms, !2622 (@alobosal)
- ~PID | Replace activation functions with faster versions and moving to floats in PID, !2611 (@lohenry) :star:
- ~"Event model" | Following changes of introduction of lhcbid_v, LHCb!3288, !2592 (@decianm)
- ~Build | Fix check-formatting to use LbEnv, !2627 (@poluekt)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking ~"Event model" | Adapt to consistent Track Enums and improve KalmanFilterTool, !2601 (@gunther)
- ~Muon ~PID | Make MuonChi2MatchTool thread-safe, !2599 (@lmeyerga)
- ~Calo | Changes to Calo code to prepare for DD4hep integration, !2584 (@sponce)
- ~Calo ~Monitoring | Calo future digit monitor changes, !2591 (@pagarcia)
- ~"Event model" ~Utilities | SOACollection proxy simplification, !2618 (@ahennequ)
- ~"MC checking" | Modernize PrPlotFTHits and use DataHandles, !2590 (@sponce)
- ~Build | Fix warning about unused cpp file, !2626 (@clemenci)
- ~Build | Minor fixes/clean up to CMake configuration, !2597 (@clemenci)
- Use prefix ++ for counter, !2617 (@clemenci)
- Shrink the ITtrackExtrapolator interface, !2609 (@graven)
- Fix: remove obsolete header files, closes #225, !2595 (@chasse) [#225]
- Adapted to new LHCb TaggedBool, !2594 (@sponce)
- Adapted code to non movable, non copyable counters, !2586 (@sponce)
- Adapted ref files to removal of GaudiAlgorithm in LHCb packers, !2514 (@sponce)
- Update References for: LHCb!3314, Run2Support!16 based on lhcb-master-mr/3124, !2613 (@lhcbsoft)
