2021-03-12 Rec v32r0
===

This version uses
Lbcom [v32r0](../../../../Lbcom/-/tags/v32r0),
LHCb [v52r0](../../../../LHCb/-/tags/v52r0),
Gaudi [v35r2](../../../../Gaudi/-/tags/v35r2) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to Rec [v31r2](/../../tags/v31r2), with the following changes:

### New features ~"new feature"

- ~Tracking | Simple Kalman Filter to fit PrTracks, !2213 (@chasse) :star:
- ~UT | UT TELL40 decoder v1r1, !2240 (@xuyuan)
- ~Muon | Removal of old codes and addition of MuonIDHlt2Alg (follow up !2292), !2296 (@sponce)
- ~Muon | Removal of old codes and addition of MuonIDHlt2Alg, !2292 (@rvazquez)
- ~Calo | Cluster/hypo-track matching algorithm with selective search in calo, !2216 (@mveghel)
- ~"MC checking" | Velo cluster monitoring and performance checks, !2337 (@gbassi) :star:


### Fixes ~"bug fix" ~workaround

- ~Tracking | Minor fix for adding UT hits to long tracks, !2347 (@peilian)
- ~Tracking | Replace hard-coded internal container size in PrForwardTracking, !2341 (@gunther)
- ~Tracking | Fix implementation of Measurement::isSameDetectorElement, !2284 (@graven)
- ~Tracking | Bug fix: Do not compute chi2 if error is zero, !2260 (@pkoppenb)
- ~Tracking | Add protections to the fixed size of temporary track container, !2241 (@peilian)
- ~Tracking ~UT | Fix stripID order in DeUTSector and DeUTSensor, !2335 (@xuyuan) [#174]
- ~Calo | MergedPi0 converter: increase number of possible overlaps to four, !2280 (@graven)
- ~RICH | RichPIDQC - Prebook histograms and explicitly define IDs, !2345 (@jonrob)
- ~RICH ~PID | RICH - Photon pre-selection reoptimisation, !2246 (@jonrob) :star:


### Enhancements ~enhancement

- ~Configuration ~Functors | Declare data dependencies in ThOr functors, !2343 (@apearce) [Moore#51]
- ~Tracking | Change assert on maximum SciFi hits to warning, !2353 (@peilian)
- ~Tracking | TrackEventFitter - Migrate to thread safe counters, !2350 (@jonrob) [#187]
- ~Tracking | Protect mutable cache in DelegatingTrackSelector, !2270 (@sponce)
- ~Tracking | Disable automatic creation of ClosestToBeam state for downstream tracks in TrackMasterFitter, !2235 (@chasse)
- ~Tracking | LongLivedTracking cleanup, optimisation and adaptation to Pr::Tracks, !2222 (@ahennequ) :star:
- ~Tracking ~"PV finding" | Flexible size of Velo tracks and hits containers and PV container, !2303 (@peilian) :star:
- ~Tracking ~UT | Add protection to the Mut UT hit container, !2262 (@peilian)
- ~Tracking ~"Event model" | Make the sizes of inner track containers flexible, !2285 (@peilian)
- ~Tracking ~"Event model" | Simplify Pr::Tracks, !2239 (@ahennequ)
- ~Tracking ~"MC checking" | Improvements to PrKalman based on further validation & Range adjustments of TrackResChecker Histos, !2300 (@chasse)
- ~Calo | Implemented delta[x,y,z,e] counters in CaloFutureShowerOverlap, !2237 (@ozenaiev)
- ~Calo ~PID | More streamlining of TMVA generated code, !2313 (@graven)
- ~Calo ~Monitoring | Update calo reco monitoring histograms, !2355 (@cmarinbe)
- ~RICH | Rich updates for ref index calib run3, !2346 (@jonrob)
- ~RICH | RICH Use derived Condition Detector element objects, !2226 (@jonrob)
- ~RICH ~PID | RICH CK Theta Resolution Optimisation, !2261 (@jonrob)
- ~Composites ~Functors | Migrate Composites' zip machinery, !2273 (@olupton)
- ~Functors | Override __nonzero__ and __bool__ to try and forbid the use of logical and/or/not in ThOr expressions, !2238 (@olupton)
- ~"MC checking" | Updated the VPHitsMonitor to use the MCHit<->Cluster linker., !2288 (@ldufour)
- ~Build | Fixed functors for DD4hep usage, !2286 (@sponce)
- ~Build | Follow changes to zip machinery, !2245 (@olupton)
- ~Build | ARM support, !2205 (@ahennequ)
- ~Build | Fixes for DD4hep compilation, !2160 (@sponce)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Adapt PyConf API to snake_case convention, !2354 (@erodrigu)
- ~Configuration | Remove useless warnings, !2348 (@rmatev)
- ~Configuration | Backward compatible changes to prepare for change in DataHandle option, !2338 (@clemenci)
- ~Configuration | Backward compatible changes to prepare for change in DataHandle option, !2297 (@clemenci) [gaudi/Gaudi#146]
- ~Tracking | TrackRungeKuttaExtrapolator: switch to meta_enum, !2358 (@graven)
- ~Tracking | Simplify TrackRungeKuttaExtrapolator, !2329 (@graven)
- ~Tracking | TrackTools: amalgamate component header files into the corresponding .cpp files, !2326 (@graven)
- ~Tracking | Measurement: remove the UTLite moniker in favour of plain UT, !2318 (@graven)
- ~Tracking | Minor tweaks to PrKalman, !2314 (@graven)
- ~Tracking | Qualify TrackCloneDataBaseTrackPtrSlice::trackptr() as rvalue reference only, !2293 (@graven)
- ~Tracking | Cleanup Tr/TrackProjector, !2279 (@graven)
- ~Tracking | Remove the need for an atomic in TrackBestTrackCreator, !2277 (@graven)
- ~Tracking | Fix #165, !2269 (@graven) [#165]
- ~Tracking ~"PV finding" ~VP ~UT ~FT ~PID ~Functors ~"MC checking" ~Monitoring | Remove Velo, TT, IT, OT, SPD, PRS, !2236 (@pkoppenb) [lhcb-dpa/project#18,lhcb-dpa/project#37] :star:
- ~Tracking ~"MC checking" | Adapt to changes required for LHCb!2956, !2361 (@graven)
- ~"PV finding" ~"Event model" | Use v2::RecVertices instead of std::vector<v2::RecVertex>, !1974 (@vrenaudi)
- ~VP ~"MC checking" | Fix clang warning (follow up !2337), !2344 (@rmatev)
- ~UT | Modernize/cleanup UT code (follow LHCb!2892), !2323 (@graven)
- ~UT | Follow changes in LHCb!2854, !2281 (@graven)
- ~Muon | Minor changes to remove hard coding of M1 station, !1862 (@pgriffit)
- ~Calo | Remove presence of spd/prs in AlignmentNtp, !2332 (@graven)
- ~Calo | Fix thread-safety of TMVA generated code used by FutureNeutralID, !2289 (@graven)
- ~Calo | Removed some counters from E correction tool, !2287 (@ozenaiev)
- ~Calo | Removed counters from L, S correction tools, !2275 (@ozenaiev)
- ~Calo | Adapted to use of reference in CaloFutureAlgUtils::ClusterFromHypo, !2266 (@sponce)
- ~Calo | Consolidate FutureClusterCovarianceMatrixTool, !2255 (@graven)
- ~Calo | Integrate SpreadEstimator and ClusterSpreadTool, !2254 (@graven)
- ~Calo | Cleanup use of CaloInterfaces, !2253 (@graven)
- ~RICH | Make RichFuture.py example work even if env vars are not defined., !2357 (@dcampora)
- ~RICH ~Monitoring | Resolve RICH-55, !2122 (@asolomin) [RICH-55]
- ~PID | Streamline TMVA generated code, !2299 (@graven)
- ~PID | Simplify ChargedProtoANNPID, !2263 (@graven)
- ~PID | Change ChargedProtoANNPId filling to not require changing existing TES content, !2247 (@graven)
- ~PID | ChargedProtoANNPID: Change 'Input' from an abstract base class to a type-erased 'handle',, !2231 (@graven)
- ~Composites | Re-organize ProtoParticle creation and ExtraInfo filling, !2227 (@graven)
- ~Functors | Use DataHandle objects in functor tests, !2349 (@apearce) [#186]
- ~"MC checking" | Remove extraneous DataHandle from IdealStateCreator, !2294 (@rmatev)
- ~"MC checking" | Simplify TrackIPResolutionChecker and decrease generated code size, !2257 (@graven)
- ~Conditions | Follow changes in LHCb!2850, !2274 (@graven)
- ~Build | Fixes for LCG 99, !2342 (@chasse)
- ~Build | Fixed clang10 warnings, !2325 (@sponce)
- ~Build | Remove no longer required backwards compatibility with range v3 version < 0.9, !2250 (@graven)
- Changes required for  lhcb/LHCb!2959, !2362 (@graven)
- Remove the use of propsPrint(), !2234 (@graven)


### Documentation ~Documentation

- Update CONTRIBUTING.md, !2258 (@cattanem)
